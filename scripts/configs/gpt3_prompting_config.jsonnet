local DATA_DIR = '../data';
local OUTPUT_DIR = 'output';

{
    'snli_train_path': DATA_DIR + '/snli_1.0/snli_1.0_train.jsonl',
    'snli_train_metrics_path': DATA_DIR + '/df_snli_metrics.pkl',
    'filtered_samples_path': DATA_DIR + '/snli_filtered_htl_ambi.csv',
    'snli_train_embeddings_path': DATA_DIR + '/snli_train_embeddings.npy',

    'output_path': OUTPUT_DIR + '/generations_htl_ambi_large.json',
    'dashboard_output_path': '../dashboard/backend/data/NLI/cartography_filtered/htl_ambi_dashboard_2000.pkl',
    'num_gens_per_prompt': 10,
    'num_incontext_examples': 5,
}
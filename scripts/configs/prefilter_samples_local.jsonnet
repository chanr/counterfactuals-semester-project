local DATA_DIR = '../data/snli_1.0';
local MODEL_DIR = '../exploration';
local MODEL_NAME = 'models_1030_gpu_full';
local OUTPUT_DIR = 'output';
local SNLI_TRAIN = 'snli_1.0_train.jsonl';

{
    'snli_train_path': DATA_DIR + '/' + SNLI_TRAIN,
    'td_args': {
        'model_dir': MODEL_DIR + '/' + MODEL_NAME,
        'model_name': MODEL_NAME,
        'plot': true,
        'plots_dir': OUTPUT_DIR + '/plots',
    },
    'cls_args': {
        'preloaded': true,
        'preloaded_file_path': 
        'fname_prefix': 'snli_train_embeddings',
        'model_dir': MODEL_DIR + '/' + MODEL_NAME
    },
    'clustering_args': {
        'preloaded': true,
        'preloaded_file_path': MODEL_DIR + '/32nn.npy'
    }
}
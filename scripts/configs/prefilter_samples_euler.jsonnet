local DATA_DIR = '/cluster/scratch/chanr/counterfactuals_data/data';
local MODEL_DIR = '/cluster/scratch/chanr/counterfactuals_data';
local MODEL_NAME = 'models_1030_gpu_full';
local OUTPUT_DIR = 'output';

local SNLI_TRAIN = 'snli_1.0_train.jsonl';

{
    'td_args': {
        'model_dir': MODEL_DIR + '/' + MODEL_NAME,
        'model_name': MODEL_NAME,
        'plot': true,
        'plots_dir': OUTPUT_DIR + '/plots',
    },
    'cls_args': {
        'preloaded': false,
        'preloaded_file_path': MODEL_DIR + '/representations/snli_train_embeddings.npy',
        'fname_prefix': 'snli_train_embeddings',
        'model_dir': MODEL_DIR + '/' + MODEL_NAME,
        'data_file': DATA_DIR + 'snli_1.0' + '/' + SNLI_TRAIN,
        'bs': 50000
    },
    'clustering_args': {
        'preloaded': false,
        'preloaded_file_path': MODEL_DIR + '/representations/32nn.npy',
        'fname': '32nn.npy',
        'output_dir': MODEL_DIR + '/representations',
    }

}
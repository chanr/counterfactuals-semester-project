import argparse
import json
import _jsonnet
import logging
import os
from pathlib import Path

import numpy as np
import pandas as pd

from utils.prefilter_samples_utils import (
    dotdict,
    get_td_metrics,
    merge_with_snli,
    filter_snli
)
from utils.cls_embedding_utils import(
    get_embeddings,
    get_nn
)

# Define logger
logging.basicConfig(
  format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config",
                    "-c",
                    type=os.path.abspath,
                    required=True,
                    help="Main config file with basic arguments.")

    args_from_cli = parser.parse_args()
    cfg_dict = json.loads(_jsonnet.evaluate_file(args_from_cli.config))
    td_args = dotdict(cfg_dict['td_args'])
    cls_args = dotdict(cfg_dict['cls_args'])
    clustering_args = dotdict(cfg_dict['cls_args'])

    # Given a trained model, get the training dynamics.
    logger.info(f'Loading Training Dyanmics of model at {td_args.model_dir}')
    _td_metrics_path = Path(td_args.model_dir) / 'td_metrics.jsonl'
    if not os.path.exists(_td_metrics_path):
        td_metrics = get_td_metrics(td_args)
    else:
        td_metrics = pd.read_json(_td_metrics_path, 
                                  orient="records", 
                                  lines=True)

    # Create CLS embeddings or load from precomputed.
    logger.info(f'Loading CLS embeddings.')
    cls_embeddings = get_embeddings(cls_args)
    # Find NNs for each sample and identify outliers.
    D, I = get_nn(cls_embeddings, clustering_args)
    # Combine SNLI data, clustering and td_metrics.
    df_snli_train_metrics = merge_with_snli(
        cfg_dict['snli_train_dir'],
        td_metrics,
        D,
        I,
        save=True
    )
    # Apply filtering step, is a bit shaky for now.
    logger.info(f'Applying filtering.')
    df_prompts = filter_snli(
        df_snli_train_metrics,
        snli_train_preds_path='../data/snli_train_ambinli_preds.pkl',
        confidence_threshold=1/3,
        ambiNLI_confidence_gap=0.6
    )
    # Save prompts to disk.
    _output_path = Path(cfg_dict['output_dir']) / 'htl_prompts.pkl'
    logger.info(f'Saving to {str(_output_path)}')
    df_prompts.to_pickle(_output_path)
    logger.info('Done!')


if __name__ == '__main__':
    main()

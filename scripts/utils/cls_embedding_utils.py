import inspect
import os
from pathlib import Path
import sys
from tqdm import tqdm

import faiss
import numpy as np
import pandas as pd
from transformers import RobertaTokenizer
import torch

# os/sys hack to import parent directory modules.
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
sys.path.append(os.path.abspath(os.path.join('..')))

from cartography.cartography.classification.models import AdaptedRobertaForSequenceClassification

def assign_GPU(Tokenizer_output):
    tokens_tensor = Tokenizer_output['input_ids'].to('cuda')
    token_type_ids = Tokenizer_output['token_type_ids'].to('cuda')
    attention_mask = Tokenizer_output['attention_mask'].to('cuda')

    output = {'input_ids' : tokens_tensor, 
          'token_type_ids' : token_type_ids, 
          'attention_mask' : attention_mask}

    return output


def get_cls_embedding(model, tokenizer, premise, hypothesis):
    if torch.cuda.is_available():
        x = assign_GPU(tokenizer.encode_plus(premise, hypothesis, return_tensors='pt', max_length=128, truncation=True))
    else:
        x = tokenizer.encode_plus(premise, hypothesis, return_tensors='pt', max_length=128, truncation=True)
    model_output = model(**x)
    assert(len(model_output[-1])==25) # assert we are actually looking at the hidden states
    return model_output[-1][-1][:,0,:]

# Run gridsearch on the number of centroids
def get_nn(cls_embeddings, args):
    if args.preloaded:
        return np.load(args.preloaded_file_path)

    index = faiss.IndexFlatL2(cls_embeddings.shape[1])
    index.add(cls_embeddings)
    D, I = index.search(cls_embeddings, 16)
    with open(
        args.output_dir,
        # '/cluster/scratch/chanr/counterfactuals_data/models_1030_gpu_full/representations/32nn.npy',
        'wb') as f:
        np.save(f, np.array([D, I]))


def get_embeddings(args):
    # Create directories.
    model_path = Path(args.model_dir)
    output_dir = model_path / 'representations'

    if args.preloaded:
        return np.load(args.preloaded_file_path)

    if not torch.cuda.is_available():
        raise Exception('CUDA is not available.')

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Load the models
    tokenizer = RobertaTokenizer.from_pretrained(model_path)
    nli_model = AdaptedRobertaForSequenceClassification.from_pretrained(model_path)
    if torch.cuda.is_available():
        nli_model = nli_model.to('cuda')

    data_df = pd.read_json(args.data_file, lines=True, orient='records')
    batch_size = args.bs
    num_batches = np.ceil(len(data_df.index) / batch_size).astype('int')
    print(f'Total number of batches: {num_batches}')

    # split into batches so we don't run out of memory
    for batch_idx in range(num_batches):
        batch = data_df[batch_idx * batch_size : min(len(data_df.index), (batch_idx+1) * batch_size)]
        with torch.no_grad():
            vectors = []
            for _, row in tqdm(batch.iterrows(), total=len(batch.index), desc=f'Batch {batch_idx}'):
                v = get_cls_embedding(nli_model, tokenizer, row['sentence1'], row['sentence2']).squeeze(0)
                vectors.append(v)

        vectors = torch.stack(vectors)
        vectors = torch.nn.functional.normalize(vectors)
        vectors = vectors.cpu().detach().numpy()
        
        # save to file
        with open(output_dir / f'{args.fname_prefix}_{batch_idx}.npy', 'wb') as fo:
            np.save(fo, vectors)

    # combine batches
    vectors_per_batch = []
    for batch_idx in range(num_batches):
        with open(output_dir / f'{args.fname_prefix}_{batch_idx}.npy', 'rb') as fin:
            vectors_per_batch.append(np.load(fin))

    embeddings_all = np.concatenate(vectors_per_batch)
    with open(output_dir / f'{args.fname_prefix}.npy', 'wb') as fo:
        np.save(fo, embeddings_all)
    return embeddings_all

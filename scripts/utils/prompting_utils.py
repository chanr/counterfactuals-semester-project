import ast
import sys, os

import openai
import pandas as pd
from tqdm import tqdm

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from API_keys import OPENAI

openai.api_key = (OPENAI)

label_to_instruction = {
    'contradiction': {
        'instruction': 'Write a pair of sentences that have the same relationship as the previous examples.',
        'label': 'Contradiction'
    },
    'entailment': {
        'instruction': 'Write a pair of sentences that have the same relationship as the previous examples.',
        'label': 'Implication'
    },
    'neutral': {
        'instruction': 'Write a pair of sentences that have the same relationship as the previous examples.',
        'label': 'Possibility'
    },
}


def format_incontext_examples(
    examples: pd.DataFrame, 
    label: str, 
):
    """
    format a given group of examples as context for GPT-3, using template for the given label
    """
    examples = examples[::-1]
    context_string = f'{label_to_instruction[label]["instruction"]} Examples:\n\n'
    # write in context examples
    for i, (_, row) in enumerate(examples.iterrows()):
        # for every chunk_size examples, repeat instructions and enumerate starting from 1
        context_string += f'{i+1}. {row["premise"]}\n{label_to_instruction[label]["label"]}: {row["hypothesis"]}\n\n'

    # write final numbering and premise, if provided
    context_string += f'{len(examples.index)+1}.'
    return context_string


def request(
    prompt: str,
    engine='text-davinci-003',
    max_tokens=120,
    temperature=1.0,
    top_p=1.0,
    n=1,
    stop='\n',
    presence_penalty=0.0,
    frequency_penalty=0.0,
    ):
    # retry request (handles connection errors, timeouts, and overloaded API)
    while True:
        try:
            response = openai.Completion.create(
                engine=engine,
                prompt=prompt,
                max_tokens=max_tokens,
                temperature=temperature,
                top_p=top_p,
                n=n,
                stop=stop,
                presence_penalty=presence_penalty,
                frequency_penalty=frequency_penalty,
            )
            break
        except Exception as e:
            tqdm.write(str(e))
            tqdm.write("Retrying...")
    
    generations = [gen['text'].lstrip() for gen in response['choices']]

    if len(generations) == 1:
        return generations[0]
    return generations


def postprocess_generations(
    snli_path: str,  # '../data/snli_1.0/snli_1.0_train.jsonl'
    snli_training_metrics_path: str,  # '../data/df_snli_metrics.pkl'
    examples_path: str,  # '../data/htl_snli_samples.csv'
    generations_path: str,  # '../scripts/output/generations_test.json'
    output_path: str  # '../dashboard/backend/data/NLI/cartography_filtered/htl_dashboard_new.pkl'
):
    snli_full = pd.read_json(snli_path, lines=True, orient='records').rename({'pairID': 'guid'}, axis=1)
    df_snli_metrics = pd.read_pickle(snli_training_metrics_path)

    examples = pd.read_csv(examples_path, index_col=0)
    generations = pd.read_json(generations_path) ## output_dir

    df_snli_metrics_indexed = df_snli_metrics.set_index('pairID')

    examples['nearest_neighbors_pairIDs'] = examples['nearest_neighbors_pairIDs'].apply(ast.literal_eval)
    examples['nearest_neighbors_labels'] = examples['nearest_neighbors_labels'].apply(ast.literal_eval)

    generations['original_pairID'] = generations.apply(lambda x:
        snli_full.guid.iloc[x.nearest_neighbors[0]], axis=1
    )

    generations['nearest_neighbors_pairIDs'] = generations.apply(
        lambda x: snli_full.loc[x.nearest_neighbors].guid.tolist(), axis=1
    )
    generations['nn_querying_pairIDs'] = generations.apply(
        lambda x: snli_full.loc[x.equal_label_nn].guid.tolist(), axis=1
    )

    generations['nn_labels'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'gold_label']
            for pid in x.nearest_neighbors_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )

    generations['nn_confidence'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'confidence']
            for pid in x.nearest_neighbors_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )

    generations['nn_variability'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'variability']
            for pid in x.nearest_neighbors_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )

    generations['nn_premises'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'sentence1']
            for pid in x.nearest_neighbors_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )
    generations['nn_hypotheses'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'sentence2']
            for pid in x.nearest_neighbors_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )

    generations['nn_querying_confidence'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'confidence']
            for pid in x.nn_querying_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )

    generations['nn_querying_variability'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'variability']
            for pid in x.nn_querying_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )

    generations['nn_querying_premises'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'sentence1']
            for pid in x.nn_querying_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )
    generations['nn_querying_hypotheses'] = generations.apply(
        lambda x: [df_snli_metrics_indexed.loc[pid, 'sentence2']
            for pid in x.nn_querying_pairIDs
            if pid in df_snli_metrics_indexed.index
        ], axis=1
    )

    generations['suggestionRP'] = ""
    generations['suggestionRP_label'] = ""
    generations['suggestionRH'] = ""
    generations['suggestionRH_label'] = ""

    # Re-format very specifically, just like the dashboard expects it.
    _generations_formatted = generations.rename(
        {'premise': 'sentence1', 'hypothesis': 'sentence2', 'label': 'gold_label'},
        axis=1
    )[['sentence1', 'sentence2', 'gold_label', 'nn_variability', 'nn_confidence',
    'nn_querying_variability', 'nn_querying_confidence', 
    'nn_premises', 'nn_hypotheses', 'nn_labels', 'nn_querying_premises', 'nn_querying_hypotheses',
    'suggestionRP', 'suggestionRP_label',
    'suggestionRH', 'suggestionRH_label']]

    print(output_path)
    print(_generations_formatted)
    _generations_formatted.to_pickle(output_path)

"""
Filtering and dataset mapping methods based on training dynamics.
By default, this module reads training dynamics from a given trained model and
computes the metrics---confidence, variability, correctness,
as well as baseline metrics of forgetfulness and threshold closeness
for each instance in the training data.
If specified, data maps can be plotted with respect to confidence and variability.
Moreover, datasets can be filtered with respect any of the other metrics.
"""
import json
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import seaborn as sns
import torch
from tqdm import tqdm
import random, re

import shutil

from collections import defaultdict
from typing import List

tqdm.pandas()

# TODO(SS): Named tuple for tasks and filtering methods.

logging.basicConfig(
  format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


def convert_string_to_unique_number(string: str) -> int:
    """
    Hack to convert SNLI ID into a unique integer ID, for tensorizing.
    """
    id_map = {'e': '0', 'c': '1', 'n': '2'}

    # SNLI-specific hacks.
    if string.startswith('vg_len'):
      code = '555'
    elif string.startswith('vg_verb'):
      code = '444'
    else:
      code = '000'

    try:
      number = int(code + re.sub(r"\D", "", string) + id_map.get(string[-1], '3'))
    except:
      number = random.randint(10000, 99999)
      logger.info(f"Cannot find ID for {string}, using random number {number}.")
    return number


def read_glue_tsv(file_path: str,
                  guid_index: int,
                  label_index: int = -1,
                  guid_as_int: bool = False):
  """
  Reads TSV files for GLUE-style text classification tasks.
  Returns:
    - a mapping between the example ID and the entire line as a string.
    - the header of the TSV file.
  """
  tsv_dict = {}

  i = -1
  with open(file_path, 'r') as tsv_file:
    for line in tqdm.tqdm([line for line in tsv_file]):
      i += 1
      if i == 0:
        header = line.strip()
        field_names = line.strip().split("\t")
        continue

      fields = line.strip().split("\t")
      label = fields[label_index]
      if len(fields) > len(field_names):
        # SNLI / MNLI fields sometimes contain multiple annotator labels.
        # Ignore all except the gold label.
        reformatted_fields = fields[:len(field_names)-1] + [label]
        assert len(reformatted_fields) == len(field_names)
        reformatted_line = "\t".join(reformatted_fields)
      else:
        reformatted_line = line.strip()

      if label == "-" or label == "":
        logger.info(f"Skippping line: {line}")
        continue

      if guid_index is None:
        guid = i
      else:
        guid = fields[guid_index] # PairID.
      if guid in tsv_dict:
        logger.info(f"Found clash in IDs ... skipping example {guid}.")
        continue
      tsv_dict[guid] = reformatted_line.strip()

  logger.info(f"Read {len(tsv_dict)} valid examples, with unique IDS, out of {i} from {file_path}")
  if guid_as_int:
    tsv_numeric = {int(convert_string_to_unique_number(k)): v for k, v in tsv_dict.items()}
    return tsv_numeric, header
  return tsv_dict, header


def copy_dev_test(task_name: str,
                  from_dir: os.path,
                  to_dir: os.path,
                  extension: str = ".tsv"):
  """
  Copies development and test sets (for data selection experiments) from `from_dir` to `to_dir`.
  """
  if task_name == "MNLI":
    dev_filename = "dev_matched.tsv"
    test_filename = "dev_mismatched.tsv"
  elif task_name in ["SNLI", "QNLI", "WINOGRANDE"]:
    dev_filename = f"dev{extension}"
    test_filename = f"test{extension}"
  else:
    raise NotImplementedError(f"Logic for {task_name} not implemented.")

  dev_path = os.path.join(from_dir, dev_filename)
  if os.path.exists(dev_path):
    shutil.copyfile(dev_path, os.path.join(to_dir, dev_filename))
  else:
    raise ValueError(f"No file found at {dev_path}")

  test_path = os.path.join(from_dir, test_filename)
  if os.path.exists(test_path):
    shutil.copyfile(test_path, os.path.join(to_dir, test_filename))
  else:
    raise ValueError(f"No file found at {test_path}")


def read_data(file_path: str, guid_as_int: bool = False):
    """
    Reads task-specific datasets from corresponding GLUE-style TSV files.
    """
    logger.warning("Data reading only works when data is in TSV format, "
                    " and last column as classification label.")

  # `guid_index`: should be 2 for SNLI, 0 for MNLI and None for any random tsv file.

    return read_glue_tsv(file_path,
                        guid_index=2,
                        guid_as_int=guid_as_int)


def read_training_dynamics(model_dir: os.path,
                           strip_last: bool = False,
                           id_field: str = "guid",
                           burn_out: int = None):
  """
  Given path to logged training dynamics, merge stats across epochs.
  Returns:
  - Dict between ID of a train instances and its gold label, and the list of logits across epochs.
  """
  train_dynamics = {}

  td_dir = os.path.join(model_dir, "training_dynamics")
  num_epochs = len([f for f in os.listdir(td_dir) if os.path.isfile(os.path.join(td_dir, f))])
  if burn_out:
    num_epochs = burn_out

  logger.info(f"Reading {num_epochs} files from {td_dir} ...")
  for epoch_num in tqdm(range(num_epochs)):
    epoch_file = os.path.join(td_dir, f"dynamics_epoch_{epoch_num}.jsonl")
    assert os.path.exists(epoch_file)

    with open(epoch_file, "r") as infile:
      for line in infile:
        record = json.loads(line.strip())
        guid = record[id_field] if not strip_last else record[id_field][:-1]
        if guid not in train_dynamics:
          assert epoch_num == 0
          train_dynamics[guid] = {"gold": record["gold"], "logits": []}
        train_dynamics[guid]["logits"].append(record[f"logits_epoch_{epoch_num}"])

  logger.info(f"Read training dynamics for {len(train_dynamics)} train instances.")
  return train_dynamics


def compute_forgetfulness(correctness_trend: List[float]) -> int:
  """
  Given a epoch-wise trend of train predictions, compute frequency with which
  an example is forgotten, i.e. predicted incorrectly _after_ being predicted correctly.
  Based on: https://arxiv.org/abs/1812.05159
  """
  if not any(correctness_trend):  # Example is never predicted correctly, or learnt!
      return 1000
  learnt = False  # Predicted correctly in the current epoch.
  times_forgotten = 0
  for is_correct in correctness_trend:
    if (not learnt and not is_correct) or (learnt and is_correct):
      # nothing changed.
      continue
    elif learnt and not is_correct:
      # Forgot after learning at some point!
      learnt = False
      times_forgotten += 1
    elif not learnt and is_correct:
      # Learnt!
      learnt = True
  return times_forgotten


def compute_correctness(trend: List[float]) -> float:
  """
  Aggregate #times an example is predicted correctly during all training epochs.
  """
  return sum(trend)


def compute_train_dy_metrics(training_dynamics, args):
    """
    Given the training dynamics (logits for each training instance across epochs), compute metrics
    based on it, for data map coorodinates.
    Computed metrics are: confidence, variability, correctness, forgetfulness, threshold_closeness---
    the last two being baselines from prior work
    (Example Forgetting: https://arxiv.org/abs/1812.05159 and
    Active Bias: https://arxiv.org/abs/1704.07433 respectively).
    Returns:
    - DataFrame with these metrics.
    - DataFrame with more typical training evaluation metrics, such as accuracy / loss.
    """
    confidence_ = {}
    variability_ = {}
    threshold_closeness_ = {}
    correctness_ = {}
    forgetfulness_ = {}
    probs_ = {}
    # Functions to be applied to the data.
    variability_func = lambda conf: np.std(conf)
    threshold_closeness_func = lambda conf: conf * (1 - conf)

    loss = torch.nn.CrossEntropyLoss()

    num_tot_epochs = len(list(training_dynamics.values())[0]["logits"])
    logger.info(f"Computing training dynamics across {num_tot_epochs} epochs")
    logger.info("Metrics computed: confidence, variability, correctness, forgetfulness, threshold_closeness")

    logits = {i: [] for i in range(num_tot_epochs)}
    targets = {i: [] for i in range(num_tot_epochs)}
    training_accuracy = defaultdict(float)

    for guid in tqdm(training_dynamics):
        correctness_trend = []
        true_probs_trend = []
        _probabilities = []

        record = training_dynamics[guid]
        print(len(logits))
        print(len(record['logits']))
        for i, epoch_logits in enumerate(record["logits"]):
            probs = torch.nn.functional.softmax(torch.Tensor(epoch_logits), dim=-1)
            true_class_prob = float(probs[record["gold"]])
            true_probs_trend.append(true_class_prob)

            prediction = np.argmax(epoch_logits)
            is_correct = (prediction == record["gold"]).item()
            correctness_trend.append(is_correct)

            training_accuracy[i] += is_correct
            _probabilities.append(probs.tolist())

            logits[i].append(epoch_logits)
            targets[i].append(record["gold"])

        probs_[guid] = np.mean(_probabilities, axis=0)
        correctness_[guid] = compute_correctness(correctness_trend)
        confidence_[guid] = np.mean(true_probs_trend)
        variability_[guid] = variability_func(true_probs_trend)

        forgetfulness_[guid] = compute_forgetfulness(correctness_trend)
        threshold_closeness_[guid] = threshold_closeness_func(confidence_[guid])

    column_names = ['guid',
                    'index',
                    'probs',
                    'threshold_closeness',
                    'confidence',
                    'variability',
                    'correctness',
                    'forgetfulness',]
    df = pd.DataFrame([[guid,
                        i,
                        probs_[guid],
                        threshold_closeness_[guid],
                        confidence_[guid],
                        variability_[guid],
                        correctness_[guid],
                        forgetfulness_[guid],
                        ] for i, guid in enumerate(correctness_)], columns=column_names)

    df_train = pd.DataFrame([[i,
                                loss(torch.Tensor(logits[i]), torch.LongTensor(targets[i])).item() / len(training_dynamics),
                                training_accuracy[i] / len(training_dynamics)
                                ] for i in range(num_tot_epochs)],
                            columns=['epoch', 'loss', 'train_acc'])
    return df, df_train


def consider_ascending_order(filtering_metric: str) -> bool:
  """
  Determine if the metric values' sorting order to get the most `valuable` examples for training.
  """
  if filtering_metric == "variability":
    return False
  elif filtering_metric == "confidence":
    return True
  elif filtering_metric == "threshold_closeness":
    return False
  elif filtering_metric == "forgetfulness":
    return False
  elif filtering_metric == "correctness":
    return True
  else:
    raise NotImplementedError(f"Filtering based on {filtering_metric} not implemented!")


def plot_data_map(dataframe: pd.DataFrame,
                  plot_dir: os.path,
                  hue_metric: str = 'correct.',
                  title: str = '',
                  model: str = 'RoBERTa',
                  show_hist: bool = False,
                  max_instances_to_plot = 55000):
    # Set style.
    sns.set(style='whitegrid', font_scale=1.6, font='Georgia', context='paper')
    logger.info(f"Plotting figure for {title} using the {model} model ...")

    # Subsample data to plot, so the plot is not too busy.
    dataframe = dataframe.sample(n=max_instances_to_plot if dataframe.shape[0] > max_instances_to_plot else len(dataframe))

    # Normalize correctness to a value between 0 and 1.
    dataframe = dataframe.assign(corr_frac = lambda d: d.correctness / d.correctness.max())
    dataframe['correct.'] = [f"{x:.1f}" for x in dataframe['corr_frac']]

    main_metric = 'variability'
    other_metric = 'confidence'

    hue = hue_metric
    num_hues = len(dataframe[hue].unique().tolist())
    style = hue_metric if num_hues < 8 else None

    if not show_hist:
        fig, ax0 = plt.subplots(1, 1, figsize=(8, 6))
    else:
        fig = plt.figure(figsize=(14, 10), )
        gs = fig.add_gridspec(3, 2, width_ratios=[5, 1])
        ax0 = fig.add_subplot(gs[:, 0])

    # Make the scatterplot.
    # Choose a palette.
    pal = sns.diverging_palette(260, 15, n=num_hues, sep=10, center="dark")

    plot = sns.scatterplot(x=main_metric,
                           y=other_metric,
                           ax=ax0,
                           data=dataframe,
                           hue=hue,
                           palette=pal,
                           style=style,
                           s=30)

    # Annotate Regions.
    bb = lambda c: dict(boxstyle="round,pad=0.3", ec=c, lw=2, fc="white")
    func_annotate = lambda  text, xyc, bbc : ax0.annotate(text,
                                                          xy=xyc,
                                                          xycoords="axes fraction",
                                                          fontsize=15,
                                                          color='black',
                                                          va="center",
                                                          ha="center",
                                                          rotation=350,
                                                           bbox=bb(bbc))
    an1 = func_annotate("ambiguous", xyc=(0.9, 0.5), bbc='black')
    an2 = func_annotate("easy-to-learn", xyc=(0.27, 0.85), bbc='r')
    an3 = func_annotate("hard-to-learn", xyc=(0.35, 0.25), bbc='b')


    if not show_hist:
        plot.legend(ncol=1, bbox_to_anchor=[0.175, 0.5], loc='right')
    else:
        plot.legend(fancybox=True, shadow=True,  ncol=1)
    plot.set_xlabel('variability')
    plot.set_ylabel('confidence')

    if show_hist:
        plot.set_title(f"{title}-{model} Data Map", fontsize=17)

        # Make the histograms.
        ax1 = fig.add_subplot(gs[0, 1])
        ax2 = fig.add_subplot(gs[1, 1])
        ax3 = fig.add_subplot(gs[2, 1])

        plott0 = dataframe.hist(column=['confidence'], ax=ax1, color='#622a87')
        plott0[0].set_title('')
        plott0[0].set_xlabel('confidence')
        plott0[0].set_ylabel('density')

        plott1 = dataframe.hist(column=['variability'], ax=ax2, color='teal')
        plott1[0].set_title('')
        plott1[0].set_xlabel('variability')
        plott1[0].set_ylabel('density')

        plot2 = sns.countplot(x="correct.", data=dataframe, ax=ax3, color='#86bf91')
        ax3.xaxis.grid(True) # Show the vertical gridlines

        plot2.set_title('')
        plot2.set_xlabel('correctness')
        plot2.set_ylabel('density')

    fig.tight_layout()
    filename = f'{plot_dir}/{title}_{model}.pdf' if show_hist else f'figures/compact_{title}_{model}.pdf'
    fig.savefig(filename, dpi=300)
    logger.info(f"Plot saved to {filename}")

# Check samples whose nearest neighbors all have the same label except for it.
def _is_outlier_label(x: pd.Series, df: pd.DataFrame, n_neighbors: int = 8) -> bool:
    _value_counts = df[df.index.isin(x.nearest_neighbors)].gold_label.value_counts()
    return (_value_counts.max() == n_neighbors - 1) and (_value_counts.idxmax() != x.gold_label)


def get_td_metrics(args: dotdict):
    training_dynamics = read_training_dynamics(args.model_dir)
    total_epochs = len(list(training_dynamics.values())[0]["logits"])
    logger.info(f"Total epochs found: {total_epochs}")
    train_dy_metrics, _ = compute_train_dy_metrics(training_dynamics, args)

    train_dy_filename = os.path.join(args.model_dir, f"td_metrics.jsonl")
    train_dy_metrics.to_json(train_dy_filename,
                            orient='records',
                            lines=True)
    logger.info(f"Metrics based on Training Dynamics written to {train_dy_filename}")

    if args.plot:
        assert args.plots_dir
        if not os.path.exists(args.plots_dir):
            os.makedirs(args.plots_dir)
        plot_data_map(train_dy_metrics, args.plots_dir, title=args.model_name, show_hist=True)
    
    return train_dy_metrics


def merge_with_snli(
                snli_train_path: str,
                td_metrics: pd.DataFrame,
                D: np.ndarray,
                I: np.ndarray,
                save: bool = True
    ):
    label_list = ['entailment', 'neutral', 'contradiction']
    # do read_json if you also want to keep the dodgy labels, maybe automatically drop them, also in the clustering.
    df_train_snli_full = pd.read_json(snli_train_path, lines=True, orient='records')
    df_train_snli_full['guid'] = df_train_snli_full['pairID'].apply(convert_string_to_unique_number)
    
    # In case we are not interested in the 8 nearest neighbors for filtering.
    if (D is not None) and (I is not None):
        df_train_snli_full['distances'] = D[:, 1:8].tolist()
        df_train_snli_full['nearest_neighbors'] = I[:, 1:8].tolist()
        # Is a sample an outlier?
        df_train_snli_full['is_outlier_label'] = df_train_snli_full.progress_apply(
                lambda x: _is_outlier_label(x, n_neighbors=8),
                axis=1
            )
        df_train_snli_full['nearest_neighbors_pairIDs'] = df_train_snli_full.progress_apply(
                lambda x: df_train_snli_full.iloc[x.nearest_neighbors].pairID.tolist(),
                axis=1
            )
        df_train_snli_full['nearest_neighbors_labels'] = df_train_snli_full.progress_apply(
                lambda x: df_train_snli_full.iloc[x.nearest_neighbors].gold_label.tolist(),
                axis=1
            )

    df_snli_metrics = td_metrics.merge(
        df_train_snli_full,
        how='left',
        on='guid'
    )
    df_snli_metrics['prediction'] = df_snli_metrics.probs.apply(lambda x: label_list[np.argmax(x)])

    if save:
        df_snli_metrics.to_pickle('df_snli_metrics.pkl')
        return df_snli_metrics
    else:
        return df_snli_metrics


def _get_annotation_distribution(l: List[str]) -> List[float]:
    assert(isinstance(l, list))
    return [
        l.count('entailment') / len(l),
        l.count('neutral') / len(l),
        l.count('contradiction') / len(l)
    ]


def filter_snli(df_snli_train_metrics: pd.DataFrame,
                snli_train_preds_path: str = None,  # '../data/snli_train_ambinli_preds.pkl'
                confidence_threshold: float = 1/3,
                ambiNLI_confidence_gap: float = 0.6) -> pd.DataFrame:

    label2idx = {'entailment': 0, 'neutral': 1, 'contradiction': 2}

    if snli_train_preds_path is not None:
      df_ambiNLI_preds = pd.read_pickle(snli_train_preds_path)
      df_ambiNLI_preds['guid'] = df_ambiNLI_preds.uid.apply(convert_string_to_unique_number)
      df_ambiNLI_preds = df_ambiNLI_preds.rename({'predicted_probabilities': 'ambiNLI_predictions'}, axis=1)

      df_snli_train_metrics = df_snli_train_metrics.merge(
          df_ambiNLI_preds[['guid', 'ambiNLI_predictions']],
          how='inner',
          on='guid'
      )
      df_snli_train_metrics['ambiNLI_confidence'] = df_snli_train_metrics.apply(
        lambda x: x.ambiNLI_predictions[label2idx[x.gold_label]], axis=1
      )

    df_snli_train_metrics['annotation_distribution'] = (
      df_snli_train_metrics.annotator_labels.apply(_get_annotation_distribution)
    )

    # First filter the samples where there are multiple labels available, and at least 3/4
    # annotators agreed uppon the label.
    df_snli_annotation_distribution = df_snli_train_metrics[
        (df_snli_train_metrics['annotator_labels'].str.len() > 1)  # more than one label available. 
        &
        df_snli_train_metrics.apply(
            lambda x: (np.max(x.annotation_distribution) > 0.74), axis=1  ## at least 3/4 annotators agree
        )
    ]

    # Hard-to-learn samples which annotators jointly disagreed with the prediction.
    df_snli_annotation_distribution = df_snli_annotation_distribution[
        (df_snli_annotation_distribution['prediction'] != df_snli_annotation_distribution['gold_label'])
        &
        (df_snli_annotation_distribution['confidence'] < confidence_threshold)
    ]

    if 'nearest_neighbors' in df_snli_train_metrics:  # whether to include cluster information.
      _output_cols = [
            'pairID', 'sentence1', 'sentence2', 'gold_label', 'annotation_distribution',
            'ambiNLI_predictions', 'prediction', 'probs',
            'confidence', 'variability', 'ambiNLI_confidence',
            'is_outlier_label', 'nearest_neighbors_pairIDs', 'nearest_neighbors_labels' 
        ]
    else:
      _output_cols = [
            'pairID', 'sentence1', 'sentence2', 'gold_label', 'annotation_distribution',
            'ambiNLI_predictions', 'prediction', 'probs',
            'confidence', 'variability', 'ambiNLI_confidence'
        ]

    if snli_train_preds_path is not None:
      # Hard-to-learn samples where AmbiNLI strongly disagrees with the prediction.
      df_snli_ambiNLI = df_snli_train_metrics[
          (
            (df_snli_train_metrics['ambiNLI_confidence'] - df_snli_train_metrics['confidence']) 
            > ambiNLI_confidence_gap
          )
          &
          (df_snli_train_metrics['confidence'] < confidence_threshold)
      ]
      return pd.concat([df_snli_annotation_distribution, df_snli_ambiNLI])[_output_cols]
    else:
      return df_snli_annotation_distribution[_output_cols]

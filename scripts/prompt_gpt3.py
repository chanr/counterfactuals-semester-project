import _jsonnet
import argparse
import json
import logging
import os
from pathlib import Path
from tqdm import tqdm
import re

import numpy as np
import pandas as pd
from scipy import spatial

from utils.prefilter_samples_utils import (
    dotdict
)
from utils.prompting_utils import(
    label_to_instruction,
    request,
    format_incontext_examples,
    postprocess_generations
)

# Define logger
logging.basicConfig(
  format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config",
                    "-c",
                    type=os.path.abspath,
                    required=True,
                    help="Main config file with basic arguments.")
    args_from_cli = parser.parse_args()
    cfg_dict = dotdict(
        json.loads(_jsonnet.evaluate_file(args_from_cli.config))
    )

    snli_full = pd.read_json(cfg_dict.snli_train_path, lines=True, orient='records').rename({'pairID': 'guid'}, axis=1)
    examples = pd.read_csv(cfg_dict.filtered_samples_path, index_col=0)
    embeddings = np.load(cfg_dict.snli_train_embeddings_path)

    generated_examples = []
    lines_per_flush = 50
    num_gens_per_prompt = cfg_dict.num_gens_per_prompt
    # write output continuously and flush periodically
    examples_fo = open(cfg_dict.output_path, 'w')

    # Compute nearest neighbors
    tree = spatial.KDTree(embeddings) 
    _examples = examples[['pairID', 'sentence1', 'sentence2', 'gold_label']].rename(
    {
        'sentence1': 'premise',
        'sentence2': 'hypothesis'
    }, axis=1
    )

    # operate on subset for testing phase.
    # _examples = _examples.iloc[:20]  ######
    _examples = _examples.iloc[20:]  # we already generated the samples for tehe first 20.

    pbar = tqdm(initial=0, total=len(_examples) * cfg_dict.num_gens_per_prompt, position=0, leave=True)
    for _, row in _examples.iterrows():

        example_id = snli_full.index[snli_full.guid == row.pairID]
        embedding = embeddings[example_id]

        for k in np.arange(4, 16):  # a quite a large space must be searched!
            neighbor_ids = tree.query(embedding, k=2**k)[1].flatten()
            _df_neighbors = snli_full.loc[
                neighbor_ids
            ].loc[snli_full['gold_label'] == row.gold_label].iloc[:cfg_dict.num_incontext_examples].rename(
                {
                    'sentence1': 'premise',
                    'sentence2': 'hypothesis'
                },
                axis=1
            )
            if len(_df_neighbors) == cfg_dict.num_incontext_examples:
                print(2**k)
                break  # we searched a large enough space.

        _context_string = format_incontext_examples(
            _df_neighbors, label=row.gold_label
        )
        
        for _ in range(num_gens_per_prompt):
            generation = request(
                    _context_string, 
                    engine='text-davinci-003',
                    max_tokens=120,
                    top_p=0.5,
                    n=1,
                    stop='\n\n',
                )

            # Try to extract a premise and hypothesis from the generation.
            try:
                premise, hypothesis = re.split(
                    rf"(?i)\n{label_to_instruction[row.gold_label]['label']}: ",
                    generation
                )
            except ValueError: # in case there are not two values returned.
                print('not successful')
                continue

            generated_ex = {
                'premise': premise,
                'hypothesis': hypothesis,
                'label': row.gold_label,
                'nearest_neighbors': neighbor_ids[:8].tolist(), # this is different to the original implementation.
                'equal_label_nn': _df_neighbors.index.tolist()
            }
            generated_examples.append(generated_ex)
            pbar.update()
            # write output
            examples_fo.write(json.dumps(generated_ex, default=str) + '\n')
            if len(generated_examples) % lines_per_flush == 0:
                examples_fo.flush()

    examples_fo.close()
    # Dump all predictions, if whole dataset has processed.
    with open(cfg_dict.output_path, 'w') as fo:
        json.dump(generated_examples, fo, indent=4)
    fo.close()

    # postprocess and save the generations to the backend.
    postprocess_generations(
        snli_path=cfg_dict.snli_train_path,
        snli_training_metrics_path=cfg_dict.snli_train_metrics_path,
        examples_path=cfg_dict.filtered_samples_path,
        generations_path=cfg_dict.output_path,
        output_path=cfg_dict.dashboard_output_path
    )


if __name__ == '__main__':
    main()
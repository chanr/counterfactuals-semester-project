import faiss
import numpy as np

# Run gridsearch on the number of centroids
def run_nn():
    print('main loaded')
    cls_embeddings = np.load(
        '/cluster/scratch/chanr/counterfactuals_data/models_mnli/representations/mnli_representations.npy'
    )
    print('loaded_embeddings')
    index = faiss.IndexFlatL2(cls_embeddings.shape[1])
    index.add(cls_embeddings)
    print(cls_embeddings.shape[0])
    D, I = index.search(cls_embeddings, cls_embeddings.shape[0]//2)
    with open(
        '/cluster/scratch/chanr/counterfactuals_data/models_mnli/representations/half_nn.npy', 'wb'
        ) as fo:
        np.save(fo, np.array([D, I]))

if __name__ == '__main__':
    print('main called')
    run_nn()

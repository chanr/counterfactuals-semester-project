#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=20
#SBATCH --gpus=1
#SBATCH --gres=gpumem:10240m
#SBATCH --time=48:00:00
#SBATCH --mem-per-cpu=12288

poetry shell
python get_CLS_embeddings.py --model_path /cluster/scratch/chanr/counterfactuals_data/models_mnli --data_file /cluster/scratch/chanr/counterfactuals_data/data/multinli_train.jsonl --dataset_name mnli_representations

# %%
from typing import List

import numpy as np
import pandas as pd
from scipy.spatial import distance
from tqdm import tqdm
tqdm.pandas()

from scipy.special import softmax
from datasets import load_dataset, load_from_disk
from transformers import BertForSequenceClassification, DistilBertForSequenceClassification, BertTokenizer, DistilBertTokenizer

from utils import convert_string_to_unique_number


BATCH_SIZE = 16
MAX_LEN = 128
LABEL_DICT = {0: "entailment", 1: "neutral", 2: "contradiction"}

# %% NOTE: requires torch@1.7.1 and transformers@2.4.1 to load properly.
# model = BertForSequenceClassification.from_pretrained('models_AmbiNLI')
# tokenizer = BertTokenizer.from_pretrained('finetuned_model')

# %% load predictions from euler
df_train_snli_full = pd.read_json('../data/snli_1.0/snli_1.0_train.jsonl', lines=True, orient='records').rename({'pairID': 'guid'}, axis=1)

# load the snli AmbiNLI preds. I saved them wrong.
df = pd.read_json('../data/AmbiNLI_preds/snli_train.json')['/cluster/scratch/chanr/counterfactuals_data/ambinli_checkpoints/finetuned_model/']
df_train_snli_preds = pd.DataFrame(df.to_list())
# load the dataset + training dynamics
df_snli_train_metrics = pd.read_pickle('../data/df_snli_metrics.pkl')

# %%
df_train_snli_preds.to_pickle('../data/snli_train_ambinli_preds.pkl')


# %%
from typing import List
from tqdm import tqdm

import numpy as np
import pandas as pd
from utils import convert_string_to_unique_number

tqdm.pandas()

def get_distribution(l: List[str]) -> List[float]:
    assert(isinstance(l, list))
    return [
        l.count('entailment') / len(l),
        l.count('neutral') / len(l),
        l.count('contradiction') / len(l)
    ]

label2idx = {'entailment': 0, 'neutral': 1, 'contradiction': 2}

df_snli_train_metrics = pd.read_pickle('../data/df_snli_metrics.pkl')
df_ambiNLI_preds = pd.read_pickle('../data/snli_train_ambinli_preds.pkl')

df_ambiNLI_preds['guid'] = df_ambiNLI_preds.uid.apply(convert_string_to_unique_number)
df_ambiNLI_preds = df_ambiNLI_preds.rename({'predicted_probabilities': 'ambiNLI_predictions'}, axis=1)

df_snli_train_metrics = df_snli_train_metrics.merge(
    df_ambiNLI_preds[['guid', 'ambiNLI_predictions']],
    how='inner',
    on='guid'
)
df_snli_train_metrics['annotation_distribution'] = df_snli_train_metrics.annotator_labels.apply(get_distribution)
df_snli_train_metrics['ambiNLI_confidence'] = df_snli_train_metrics.apply(
    lambda x: x.ambiNLI_predictions[label2idx[x.gold_label]], axis=1
)

# First filter the samples where there are multiple labels available, and at least 3/4
# annotators agreed uppon the label.

confidence_threshold = 1/3  # lower-bound on the confidence to classify as hard-to-learn.

df_snli_annotation_distribution = df_snli_train_metrics[
    (df_snli_train_metrics['annotator_labels'].str.len() > 1)
    &
    df_snli_train_metrics.apply(
        lambda x: (np.max(x.annotation_distribution) > 0.74), axis=1  ## at least 3/4 annotators agree
    )
]

# Hard-to-learn samples which annotators jointly disagreed with the prediction.
df_snli_annotation_distribution = df_snli_annotation_distribution[
    (df_snli_annotation_distribution['prediction'] != df_snli_annotation_distribution['gold_label'])
    &
    (df_snli_annotation_distribution['confidence'] < confidence_threshold)
]

# Hard-to-learn samples where AmbiNLI strongly disagrees with the prediction.
df_snli_ambiNLI = df_snli_train_metrics[
    ((df_snli_train_metrics['ambiNLI_confidence'] - df_snli_train_metrics['confidence']) > 0.6)
    &
    (df_snli_train_metrics['confidence'] < confidence_threshold)
]

pd.concat(
    [df_snli_annotation_distribution, df_snli_ambiNLI]
)[[
    'pairID', 'sentence1', 'sentence2', 'gold_label', 'annotation_distribution',
    'ambiNLI_predictions', 'prediction', 'probs',
    'confidence', 'variability', 'ambiNLI_confidence',
    'is_outlier_label', 'nearest_neighbors_pairIDs', 'nearest_neighbors_labels' 
]]

# %%
pd.options.display.max_colwidth = 100  # set a value as your need
df_snli_annotation_distribution.sort_values('confidence')[
    ['sentence1', 'sentence2', 'gold_label', 'annotation_distribution', 'prediction', 'confidence', 'is_outlier_label']
].is_outlier_label.value_counts(normalize=True)

# %%
df_snli_ambiNLI.sort_values('confidence')[
        ['sentence1', 'sentence2', 'gold_label', 'annotation_distribution', 'prediction', 'confidence', 'is_outlier_label']
].is_outlier_label.value_counts(normalize=True)

# %%
df_snli_train_metrics.is_outlier_label.value_counts(normalize=True)

# %%
pd.concat(
    [df_snli_annotation_distribution, df_snli_ambiNLI]
)[[
    'pairID', 'sentence1', 'sentence2', 'gold_label', 'annotation_distribution',
    'ambiNLI_predictions', 'prediction', 'probs',
    'confidence', 'variability', 'ambiNLI_confidence',
    'is_outlier_label', 'nearest_neighbors_pairIDs', 'nearest_neighbors_labels' 
]].to_csv('../data/htl_snli_samples.csv')

# %% We actually want them to be in hard-to-learn, and the confidence to be large.
pd.concat(
    [df_snli_annotation_distribution, df_snli_ambiNLI]
).to_csv('htl_snli_samples.csv')


# %%
df_snli_train_metrics.apply(
    lambda x: np.argmax(x.probs) != np.argmax(x.predicted_probabilities),
    axis=1).value_counts(normalize=True)

# %% drop some columns. 
df_snli = df_snli_train_metrics[
    ['uid', 'sentence1', 'sentence2', 'gold_label',
    'annotator_labels',
    'probs', 'prediction', 'predicted_probabilities', 
    'is_outlier_label', 'nearest_neighbors_pairIDs', 'nearest_neighbors_labels',
    'confidence', 'variability'
    ]
]

# %% Computing the JSD doesn't look great overall.

df_disagreement = df_snli[
    df_snli.apply(
    lambda x: np.argmax(x.probs) != np.argmax(x.predicted_probabilities),
    axis=1)
] # training samples where the model predictions disagree.
jsd_roberta = df_disagreement.apply(lambda x: distance.jensenshannon(x.probs, x.annotation_distribution), axis=1)
jsd_ambinli = df_disagreement.apply(lambda x: distance.jensenshannon(x.predicted_probabilities, x.annotation_distribution), axis=1)

import argparse, json, _jsonnet, os, sys, inspect, re, random
import pandas as pd
import torch
import multiprocessing as mp
from typing import List, Dict
from functools import partial
import numpy as np

# os/sys hack to import parent directory modules.
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from transformers.data.processors.utils import InputFeatures
from transformers import glue_output_modes
from transformers import RobertaTokenizer
from transformers.data.processors.utils import InputExample
from cartography.cartography.classification.models import AdaptedRobertaForSequenceClassification

import logging
logging.basicConfig(
  format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def convert_string_to_unique_number(string: str) -> int:
  """
  Hack to convert SNLI ID into a unique integer ID, for tensorizing.
  """
  id_map = {'e': '0', 'c': '1', 'n': '2'}

  # SNLI-specific hacks.
  if string.startswith('vg_len'):
    code = '555'
  elif string.startswith('vg_verb'):
    code = '444'
  else:
    code = '000'

  try:
    number = int(code + re.sub(r"\D", "", string) + id_map.get(string[-1], '3'))
  except:
    number = random.randint(10000, 99999)
    logger.info(f"Cannot find ID for {string}, using random number {number}.")
  return number


class AdaptedInputFeatures(InputFeatures):
    def __init__(self, input_ids, attention_mask=None, token_type_ids=None, label=None, example_id=None):
        self.input_ids = input_ids
        self.attention_mask = attention_mask
        self.token_type_ids = token_type_ids
        self.label = label
        self.example_id = example_id


def adapted_glue_convert_examples_to_features(
    examples,
    tokenizer,
    max_length=512,
    task=None,
    label_list=None,
    output_mode=None,
    pad_on_left=False,
    pad_token=0,
    pad_token_segment_id=0,
    mask_padding_with_zero=True,
):
    """
    Adapted from `transformers`. New functionality: also return an integer ID for each example.
    Loads a data file into a list of ``InputFeatures``

    Args:
        examples: List of ``InputExamples`` or ``tf.data.Dataset`` containing the examples.
        tokenizer: Instance of a tokenizer that will tokenize the examples
        max_length: Maximum example length
        task: GLUE task
        label_list: List of labels. Can be obtained from the processor using the ``processor.get_labels()`` method
        output_mode: String indicating the output mode. Either ``regression`` or ``classification``
        pad_on_left: If set to ``True``, the examples will be padded on the left rather than on the right (default)
        pad_token: Padding token
        pad_token_segment_id: The segment ID for the padding token (It is usually 0, but can vary such as for XLNet where it is 4)
        mask_padding_with_zero: If set to ``True``, the attention mask will be filled by ``1`` for actual values
            and by ``0`` for padded values. If set to ``False``, inverts it (``1`` for padded values, ``0`` for
            actual values)

    Returns:
        If the ``examples`` input is a ``tf.data.Dataset``, will return a ``tf.data.Dataset``
        containing the task-specific features. If the input is a list of ``InputExamples``, will return
        a list of task-specific ``InputFeatures`` which can be fed to the model.

    """

    if task is not None:
        if label_list is None:
            label_list = ['entailment', 'neutral', 'contradiction']
            logger.info("Using label list %s for task %s" % (label_list, task))
        if output_mode is None:
            output_mode = glue_output_modes[task]
            logger.info("Using output mode %s for task %s" % (output_mode, task))

    features = []
    for (ex_index, example) in enumerate(examples):
        len_examples = len(examples)
        if ex_index % 10000 == 0:
            logger.info("Writing example %d/%d" % (ex_index, len_examples))

        inputs = tokenizer.encode_plus(example.text_a, example.text_b, add_special_tokens=True, max_length=max_length,)
        input_ids, token_type_ids = inputs["input_ids"], inputs["token_type_ids"]

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        attention_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)

        # Zero-pad up to the sequence length.
        padding_length = max_length - len(input_ids)
        if pad_on_left:
            input_ids = ([pad_token] * padding_length) + input_ids
            attention_mask = ([0 if mask_padding_with_zero else 1] * padding_length) + attention_mask
            token_type_ids = ([pad_token_segment_id] * padding_length) + token_type_ids
        else:
            input_ids = input_ids + ([pad_token] * padding_length)
            attention_mask = attention_mask + ([0 if mask_padding_with_zero else 1] * padding_length)
            token_type_ids = token_type_ids + ([pad_token_segment_id] * padding_length)

        assert len(input_ids) == max_length, "Error with input length {} vs {}".format(len(input_ids), max_length)
        assert len(attention_mask) == max_length, "Error with input length {} vs {}".format(
            len(attention_mask), max_length
        )
        assert len(token_type_ids) == max_length, "Error with input length {} vs {}".format(
            len(token_type_ids), max_length
        )

        example_int_id = convert_string_to_unique_number(example.guid)

        features.append(
            AdaptedInputFeatures(input_ids=input_ids,
                                 attention_mask=attention_mask,
                                 token_type_ids=token_type_ids,
                                 example_id=example_int_id))

    return features


def predict_single_proba(sentence1: str, sentence2: str, model, tokenizer) -> torch.tensor:
    examples = [InputExample(
        guid='1',
        text_a=sentence1,
        text_b=sentence2,
    )]

    features = adapted_glue_convert_examples_to_features(
        examples,
        tokenizer=tokenizer,
        label_list=['entailment', 'neutral', 'contradiction'],
        max_length=512,
        output_mode='classification',
        pad_on_left=False,
        pad_token=tokenizer.convert_tokens_to_ids([tokenizer.pad_token])[0],
        pad_token_segment_id=0
    )

    all_input_ids = torch.tensor([f.input_ids for f in features], dtype=torch.long)
    all_attention_mask = torch.tensor([f.attention_mask for f in features], dtype=torch.long)

    outputs = model(input_ids=all_input_ids, attention_mask=all_attention_mask)
    return torch.nn.functional.softmax(outputs[0].detach(), dim=1)


def xpredict_eval_set(eval_samples: List[Dict], model, tokenizer, return_proba: bool = False) -> torch.tensor:
    examples = [InputExample(
        guid=row['guid'],
        text_a=row['sentence1'],
        text_b=row['sentence2'],
    ) for row in eval_samples]

    features = adapted_glue_convert_examples_to_features(
        examples,
        tokenizer=tokenizer,
        label_list=['entailment', 'neutral', 'contradiction'],
        max_length=512,
        output_mode='classification',
        pad_on_left=False,
        pad_token=tokenizer.convert_tokens_to_ids([tokenizer.pad_token])[0],
        pad_token_segment_id=0
    )
    all_input_ids = torch.tensor([f.input_ids for f in features], dtype=torch.long)
    all_attention_mask = torch.tensor([f.attention_mask for f in features], dtype=torch.long)
    model.eval()
    with torch.no_grad():
        outputs = model(input_ids=all_input_ids, attention_mask=all_attention_mask)
        if return_proba:
            return torch.nn.functional.softmax(outputs[0].detach(), dim=1)
        else:
            return outputs

def run_eval(args):
    logger.info(f'CPU count: {mp.cpu_count()}')
    model = AdaptedRobertaForSequenceClassification.from_pretrained(
        args['model_path']
    )
    tokenizer = RobertaTokenizer.from_pretrained('roberta-large')
    hans_eval_samples = (
        pd.read_json(
            args['hans_eval_set_path'],
            orient='records',
            lines=True
        )
        .rename({'gold_label': 'label', 'pairID': 'guid'}, axis=1)
        [['guid', 'sentence1', 'sentence2', 'label']]
        .to_dict('records')
    )

    # logger.info(hans_eval_samples[:20])
    multiprocess = False
    if multiprocess:
        with mp.Pool() as pool:
            preds = pool.map(partial(predict_eval_set, model=model, tokenizer=tokenizer), np.array_split(hans_eval_samples, mp.cpu_count()))
        torch.save(preds, args['output_file'])
    else:
        preds = predict_eval_set(hans_eval_samples[:2], model, tokenizer)

        # print(len(preds), preds)
        # torch.save(preds, args['output_file'])


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--config",
                        "-c",
                        type=os.path.abspath,
                        required=True,
                        help="Main config file with basic arguments.")

    args_from_cli = parser.parse_args()
    args = json.loads(_jsonnet.evaluate_file(args_from_cli.config))
    run_eval(args)


if __name__ == '__main__':
    main()

# %%
import glob
from tqdm import tqdm
from transformers import RobertaForSequenceClassification, RobertaTokenizer

import numpy as np
import pandas as pd
import torch
# %%  check paths.

# %% what sentencepair to evaluate
evaluation_df = pd.DataFrame(
    {
        'premise': ['I am walking alongside a river.'],
        'hypothesis': ['I am walking.'],
        'label': ['entailment'] 
    }
)

# %%
model_path = 'models_0301_checkpoints'
device = 'cpu'

checkpoints = sorted(glob.glob(f'{model_path}/checkpoint-*'))
epochs = len(checkpoints)

model_checkpoints = {}
# load tokenizer!
tokenizer = RobertaTokenizer.from_pretrained(
    checkpoints[0]
)
for e in tqdm(range(epochs), desc=f'Loading all models'):
    _model_checkpoint = RobertaForSequenceClassification.from_pretrained(
        checkpoints[e]
    )
    model_checkpoints[f'epoch_{e}'] = _model_checkpoint

# %%
def process_encode_plus_output(encoded_input, max_length=512):
    input_ids, token_type_ids, attention_mask = (
    encoded_input["input_ids"], encoded_input["token_type_ids"], 
    encoded_input['attention_mask']
    )
    # The mask has 1 for real tokens and 0 for padding tokens. Only real
    # tokens are attended to.
    padding_length = max_length - len(input_ids)

    input_ids = input_ids + ([0] * padding_length)
    attention_mask = attention_mask + ([0] * padding_length)
    token_type_ids = token_type_ids + ([0] * padding_length)

    all_input_ids = torch.tensor([input_ids], dtype=torch.long)
    all_attention_mask = torch.tensor([attention_mask], dtype=torch.long)
    return {'input_ids': all_input_ids, 'attention_mask': all_attention_mask}

# %%
LABEL2ID = {'entailment': 0, 'neutral': 1, 'contradiction': 2}
logits = []
for model in tqdm(model_checkpoints.values()):
    for i, row in evaluation_df.iterrows():
        sentence1, sentence2, label = row['premise'], row['hypothesis'], row['label']
        max_length = 512
        encoded_input = tokenizer.encode_plus(sentence1, sentence2, add_special_tokens=True, max_length=max_length,)
        logits.append(model(
            **process_encode_plus_output(encoded_input=encoded_input)
            )[0].detach()
        )
    # get logits for each model.
    probs = torch.nn.functional.softmax(torch.cat(logits), dim=1).numpy()[:, LABEL2ID[label]]
    est_confidence = np.mean(probs)
    variability_func = lambda conf: np.sqrt(np.var(conf) + np.var(conf) * np.var(conf) / (len(conf)-1))
    est_variability = variability_func(probs)

# %% compute the datamap estimates.
list(range(epochs))[::2]
# %%

def estimate_datamap_coordinates(model_checkpoints, row, use_less_checkpoints: bool = False):
    if use_less_checkpoints:
        model_checkpoints = model_checkpoints[::2]  # only use every second checkpoint

    LABEL2ID = {'entailment': 0, 'neutral': 1, 'contradiction': 2}
    logits = []
    max_length = 512
    

    for model in tqdm(model_checkpoints.values()):
        sentence1, sentence2, label = row['premise'], row['hypothesis'], row['label']
        encoded_input = tokenizer.encode_plus(sentence1, sentence2, add_special_tokens=True, max_length=max_length,)
        logits.append(model(
            **process_encode_plus_output(encoded_input=encoded_input)
            )[0].detach()
        )
        # get logits for each model.
        probs = torch.nn.functional.softmax(torch.cat(logits), dim=1).numpy()[:, LABEL2ID[label]]
        est_confidence = np.mean(probs)
        variability_func = lambda conf: np.sqrt(np.var(conf) + np.var(conf) * np.var(conf) / (len(conf)-1))
        est_variability = variability_func(probs)
    
    return [est_variability, est_confidence]




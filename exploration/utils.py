import logging
import re
from typing import List, Dict
import json, random

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import torch


def assign_GPU(tokenizer_output: torch.tensor) -> Dict:
    tokens_tensor = tokenizer_output['input_ids'].to('cuda')
    token_type_ids = tokenizer_output['token_type_ids'].to('cuda')
    attention_mask = tokenizer_output['attention_mask'].to('cuda')

    output = {'input_ids' : tokens_tensor, 
          'token_type_ids' : token_type_ids, 
          'attention_mask' : attention_mask}

    return output


def get_cls_embedding(model, tokenizer, premise, hypothesis, return_output: bool = True) -> np.ndarray:
    if torch.cuda.is_available():
        x = assign_GPU(tokenizer.encode_plus(premise, hypothesis, return_tensors='pt', max_length=128, truncation=True))
    else:
        x = tokenizer.encode_plus(premise, hypothesis, return_tensors='pt', max_length=128, truncation=True)
    model_output = model(**x)
    if return_output:
        return model_output
    else:  # else just return CLS embedding
        return model_output[-1][-1][:,0,:]


def create_examples(snli_data_path: str,
                     two_class: bool = False,
                     n_hans_samples_per_class: int = 0,
                     hans_data_path: str = None,
                     seed: int = None) -> List[Dict]:
    """Creates examples for the training and dev sets."""
    examples = []
    with open(snli_data_path, 'r', encoding='utf-8') as file:
        for line in file:
            _json_obj = json.loads(line)
            guid = _json_obj['pairID']
            text_a = _json_obj['sentence1']
            text_b = _json_obj['sentence2']
            label = _json_obj['gold_label']
            if (label == '-') or (label == ''):
                continue
            if two_class:
                if label == 'contradiction' or label == 'neutral':
                    label = 'non-entailment'

            examples.append(
                {
                    'guid': guid,
                    'sentence1': text_a,
                    'sentence2': text_b,
                    'label': label   
                })
        if n_hans_samples_per_class != 0:
            # Assert the SNLI samples are two-class. 
            if not two_class:
                raise ValueError('Hans samples are two-class. Set `two_class=True`.')
            # First, drop same-class samples randomly:
            random.seed(seed)
            _indices_del = (
                random.sample([i for i, sample_dict in enumerate(examples)
                    if sample_dict['label'] == 'entailment'], n_hans_samples_per_class) 
                + random.sample([i for i, sample_dict in enumerate(examples)
                    if sample_dict['label'] == 'non-entailment'], n_hans_samples_per_class)
            )
            # Load random samples from the HANS dataset equally from both classes.
            new_samples = (
                pd.read_json(
                    hans_data_path,
                    orient='records',
                    lines=True
                )
                .groupby('gold_label')
                .sample(n=n_hans_samples_per_class)
                .rename({'gold_label': 'label', 'pairID': 'guid'}, axis=1)
                [['guid', 'sentence1', 'sentence2', 'label']]
                .to_dict('records')
            )

            # Replace them directly.
            for i, new_sample in zip(_indices_del, new_samples):
                examples[i] = new_sample

    return examples 


def joint_it(dataframe, hue_metric, title='', model='RoBERTa', do_equal_sampling=False) -> None:
    # Subsample data to plot, so the plot is not too busy.
    dataframe = dataframe.sample(n=25000 if dataframe.shape[0] > 25000 else len(dataframe))
    
    # Normalize correctness to a value between 0 and 1.
    dataframe = dataframe.assign(corr_frac = lambda d: d.correctness / d.correctness.max())
    dataframe['correct.'] = [f"{x:.1f}" for x in dataframe['corr_frac']]
    
    main_metric = 'variability'
    other_metric = 'confidence'
    
    hue = hue_metric
    num_hues = len(dataframe[hue].unique().tolist())
    
    # Choose a palette.
    pal = sns.diverging_palette(260, 15, n=num_hues, sep=10, center="dark")

    if do_equal_sampling:
        dataframe = (
            dataframe
            .groupby(hue_metric)
            .sample(dataframe[hue_metric].value_counts()[True])
        )


    sns.jointplot(x=main_metric,
                         y=other_metric,
                         data=dataframe,
                         hue=hue,
                         kind='kde',
                         palette=pal)

# Function for plotting datamap
def scatter_it(dataframe, hue_metric ='correct.', title='', model='RoBERTa', show_hist=False):
    # Subsample data to plot, so the plot is not too busy.
    dataframe = dataframe.sample(n=25000 if dataframe.shape[0] > 25000 else len(dataframe))
    
    # Normalize correctness to a value between 0 and 1.
    dataframe = dataframe.assign(corr_frac = lambda d: d.correctness / d.correctness.max())
    dataframe['correct.'] = [f"{x:.1f}" for x in dataframe['corr_frac']]
    
    main_metric = 'variability'
    other_metric = 'confidence'
    
    hue = hue_metric
    num_hues = len(dataframe[hue].unique().tolist())
    style = hue_metric if num_hues < 8 else None

    if not show_hist:
        fig, axs = plt.subplots(1, 1, figsize=(8, 4))
        ax0 = axs
    else:
        fig = plt.figure(figsize=(16, 10), )
        gs = fig.add_gridspec(2, 3, height_ratios=[5, 1])
    
        ax0 = fig.add_subplot(gs[0, :])
    
    ### Make the scatterplot.
    
    # Choose a palette.
    pal = sns.diverging_palette(260, 15, n=num_hues, sep=10, center="dark")

    plot = sns.scatterplot(x=main_metric,
                           y=other_metric,
                           ax=ax0,
                           data=dataframe,
                           hue=hue,
                        #    size='hans_type',
                        #    sizes={'non-hans': 5, 'subsequence': 100, 'lexical_overlap': 250, 'constituent': 400},
                           palette=pal,
                           style=style,
                           s=30)
    
    # Annotate Regions.
    bb = lambda c: dict(boxstyle="round,pad=0.3", ec=c, lw=2, fc="white")
    an1 = ax0.annotate("ambiguous", xy=(0.9, 0.5), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", rotation=350, bbox=bb('black'))
    an2 = ax0.annotate("easy-to-learn", xy=(0.27, 0.85), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", bbox=bb('r'))
    an3 = ax0.annotate("hard-to-learn", xy=(0.35, 0.25), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", bbox=bb('b'))
    
    if not show_hist:
        plot.legend(ncol=1, bbox_to_anchor=(1.01, 0.5), loc='center left', fancybox=True, shadow=True)
    else:
        plot.legend(fancybox=True, shadow=True,  ncol=1)
    plot.set_xlabel('variability')
    plot.set_ylabel('confidence')
    
    if show_hist:
        plot.set_title(f"{model}-{title} Data Map", fontsize=17)
        
        # Make the histograms.
        ax1 = fig.add_subplot(gs[1, 0])
        ax2 = fig.add_subplot(gs[1, 1])
        ax3 = fig.add_subplot(gs[1, 2])

        plott0 = dataframe.hist(column=['confidence'], ax=ax1, color='#622a87')
        plott0[0].set_title('')
        plott0[0].set_xlabel('confidence')
        plott0[0].set_ylabel('density')

        plott1 = dataframe.hist(column=['variability'], ax=ax2, color='teal')
        plott1[0].set_title('')
        plott1[0].set_xlabel('variability')

        plot2 = sns.countplot(x="correct.", data=dataframe, color='#86bf91', ax=ax3)
        ax3.xaxis.grid(True) # Show the vertical gridlines

        plot2.set_title('')
        plot2.set_xlabel('correctness')
        plot2.set_ylabel('')


    fig.tight_layout()

def convert_string_to_unique_number(string: str) -> int:
  """
  Hack to convert SNLI ID into a unique integer ID, for tensorizing.
  """
  id_map = {'e': '0', 'c': '1', 'n': '2'}

  # SNLI-specific hacks.
  if string.startswith('vg_len'):
    code = '555'
  elif string.startswith('vg_verb'):
    code = '444'
  else:
    code = '000'

  try:
    number = int(code + re.sub(r"\D", "", string) + id_map.get(string[-1], '3'))
  except:
    number = random.randint(10000, 99999)
    # logger.info(f"Cannot find ID for {string}, using random number {number}.")
  return number
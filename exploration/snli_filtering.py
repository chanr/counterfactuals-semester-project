# %%
import numpy as np
import pandas as pd

df_snli_metrics = pd.read_pickle('../data/df_snli_metrics.pkl')
outlier_samples = df_snli_metrics[df_snli_metrics.is_outlier_label]

# %% Get those samples where we have more than one label available.
df_snli_multi_labeled = df_snli_metrics[
    df_snli_metrics.annotator_labels.str.len() > 1
]

# %%
df_snli_multi_labeled[(df_snli_multi_labeled.apply(
    lambda x: np.mean([nn_label == x.gold_label for nn_label in x.nearest_neighbors_labels]),
    axis=1
) < 0.4)]
# %%
df_filtered = df_snli_multi_labeled[((df_snli_multi_labeled.apply(
    lambda x: np.mean([nn_label == x.gold_label for nn_label in x.nearest_neighbors_labels]),
    axis=1
) < 0.4) & (df_snli_multi_labeled.apply(
    lambda x: np.mean([a_label == x.gold_label for a_label in x.annotator_labels]),
    axis=1
) >  0.74)) & (
    df_snli_multi_labeled.confidence < 0.4
)
]

# %%
df_filtered

# %%
pd.set_option('display.max_colwidth', None)
df_filtered[
    ['pairID', 'sentence1',
    'sentence2', 'gold_label',
    'nearest_neighbors_pairIDs',
    'nearest_neighbors_labels'
    ]].to_csv('../data/snli_filtered_htl_ambi.csv')


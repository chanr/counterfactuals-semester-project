# %% load WaNLI and MNLI models from huggingface.
# import os
# os.environ["HF_ENDPOINT"] = "https://huggingface.co"
# from huggingface_hub import hf_hub_download
# hf_hub_download(repo_id='alisawuffles/roberta-large-wanli', filename)


# %%
import transformers
import torch
from transformers import RobertaTokenizer, RobertaForSequenceClassification
print(transformers.__version__)
model = RobertaForSequenceClassification.from_pretrained('alisawuffles/roberta-large-wanli')
tokenizer = RobertaTokenizer.from_pretrained('alisawuffles/roberta-large-wanli')

# %%
x = tokenizer("A wrestler is jumping off of the ring to hit his competitor.", "Two men are competing in a wrestling match.", return_tensors='pt', max_length=128, truncation=True)
logits = model(**x).logits
probs = logits.softmax(dim=1).squeeze(0)
label_id = torch.argmax(probs).item()
prediction = model.config.id2label[label_id]
# %%
import pandas as pd
df_ncf = pd.read_csv('NCF_samples.csv')

# %%
predictions = []
for index, row in df_ncf.iterrows():
    x = tokenizer(row.sentence1, row.sentence2, return_tensors='pt', max_length=128, truncation=True)
    logits = model(**x).logits
    probs = logits.softmax(dim=1).squeeze(0)
    label_id = torch.argmax(probs).item()
    predictions.append(model.config.id2label[label_id])
df_ncf['wanli_predictions'] = predictions
df_ncf.apply(
    lambda x: x.gold_label == x.wanli_predictions, axis=1 
).mean()
# %%

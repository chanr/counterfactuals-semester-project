import faiss

import pandas as pd
import numpy as np

from sklearn.metrics import silhouette_score

# Run gridsearch on the number of centroids
def run_clustering_gridsearch():
    cls_embeddings = np.load(
        '/cluster/scratch/chanr/counterfactuals_data/models_1030_gpu_full/representations/snli_train_embeddings.npy')
    silhouette_scores = []
    clusters = []
    n_centroids_list = [8]
    d = cls_embeddings.shape[1]
    for n_centroids in n_centroids_list:
        kmeans = faiss.Kmeans(d, n_centroids, verbose=False)
        kmeans.train(cls_embeddings)
        print('Computing silhouette scores...')
        D, I = kmeans.index.search(cls_embeddings, 1)
        print(f'Appending silhouette scores for {n_centroids} centroids.')
        silh_score = silhouette_score(cls_embeddings, np.ravel(I))
        clusters.append((n_centroids, D, I, silh_score))
        silhouette_scores.append(silh_score)

    with open('/cluster/scratch/chanr/counterfactuals_data/models_1030_gpu_full/representations/best_clusters.npy', 'wb') as fo:
        np.save(fo, np.array(clusters[np.argmax(silhouette_scores)]))
    with open('/cluster/scratch/chanr/counterfactuals_data/models_1030_gpu_full/representations/all_clusters.npy', 'wb') as fo:
        np.save(fo, clusters)

if __name__ == '__main__':
    run_clustering_gridsearch()

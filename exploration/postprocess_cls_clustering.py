# %%
from ast import literal_eval

import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm 
import pandas as pd
import os, sys, inspect
import numpy as np
from tqdm import tqdm

tqdm.pandas()

from sklearn.metrics import silhouette_score

# os/sys hack to import parent directory modules.
tqdm.pandas()
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from utils import create_examples
from cartography.cartography.data_utils_glue import convert_string_to_unique_number


# %% Load original SNLI data.
df_train_snli = pd.DataFrame(
    create_examples('../data/snli_1.0/snli_1.0_train.jsonl')
    )
cls_embeddings = np.load('models_1030_gpu_full/snli_train_embeddings.npy')

# %% try to visualize where these points are on the datamap.
def scatter_it(dataframe, hue_metric ='correct.', title='', model='RoBERTa', show_hist=False):
    # Subsample data to plot, so the plot is not too busy.
    dataframe = dataframe.sample(n=25000 if dataframe.shape[0] > 25000 else len(dataframe))
    
    # Normalize correctness to a value between 0 and 1.
    dataframe = dataframe.assign(corr_frac = lambda d: d.correctness / d.correctness.max())
    dataframe['correct.'] = [f"{x:.1f}" for x in dataframe['corr_frac']]
    
    main_metric = 'variability'
    other_metric = 'confidence'
    
    hue = hue_metric
    num_hues = len(dataframe[hue].unique().tolist())
    style = hue_metric if num_hues < 8 else None
    print(style)

    if not show_hist:
        fig, axs = plt.subplots(1, 1, figsize=(8, 4))
        ax0 = axs
    else:
        fig = plt.figure(figsize=(16, 10), )
        gs = fig.add_gridspec(2, 3, height_ratios=[5, 1])
    
        ax0 = fig.add_subplot(gs[0, :])

    ### Make the scatterplot.
    
    # Choose a palette.
    pal = sns.diverging_palette(260, 15, n=num_hues, sep=10, center="dark")

    plot = sns.scatterplot(x=main_metric,
                           y=other_metric,
                           ax=ax0,
                           data=dataframe,
                           hue=hue,
                           size=hue,
                           sizes={True: 100, False: 10},
                           palette=pal,
                           style=style,
                           s=30)
    
    # Annotate Regions.
    bb = lambda c: dict(boxstyle="round,pad=0.3", ec=c, lw=2, fc="white")
    an1 = ax0.annotate("ambiguous", xy=(0.9, 0.5), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", rotation=350, bbox=bb('black'))
    an2 = ax0.annotate("easy-to-learn", xy=(0.27, 0.85), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", bbox=bb('r'))
    an3 = ax0.annotate("hard-to-learn", xy=(0.35, 0.25), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", bbox=bb('b'))
    
    if not show_hist:
        plot.legend(ncol=1, bbox_to_anchor=(1.01, 0.5), loc='center left', fancybox=True, shadow=True)
    else:
        plot.legend(fancybox=True, shadow=True,  ncol=1)
    plot.set_xlabel('variability')
    plot.set_ylabel('confidence')
    
    if show_hist:
        plot.set_title(f"{model}-{title} Data Map", fontsize=17)
        
        # Make the histograms.
        ax1 = fig.add_subplot(gs[1, 0])
        ax2 = fig.add_subplot(gs[1, 1])
        ax3 = fig.add_subplot(gs[1, 2])

        plott0 = dataframe.hist(column=['confidence'], ax=ax1, color='#622a87')
        plott0[0].set_title('')
        plott0[0].set_xlabel('confidence')
        plott0[0].set_ylabel('density')

        plott1 = dataframe.hist(column=['variability'], ax=ax2, color='teal')
        plott1[0].set_title('')
        plott1[0].set_xlabel('variability')

        plot2 = sns.countplot(x="correct.", data=dataframe, color='#86bf91', ax=ax3)
        ax3.xaxis.grid(True) # Show the vertical gridlines

        plot2.set_title('')
        plot2.set_xlabel('correctness')
        plot2.set_ylabel('')


    fig.tight_layout()
    # filename = f'figures/{title}_{model}.pdf' if show_hist else f'figures/compact_{title}_{model}.pdf'
    # fig.savefig('cls_cluster_outliers.pdf', dpi=300)

def get_neighbor_sentences(l1: list) -> list:
    return [
        [
            df_snli_metrics.loc[item].sentence1,
            df_snli_metrics.loc[item].sentence2,
            df_snli_metrics.loc[item].gold_label, 
            df_snli_metrics.loc[item].prediction
        ] for item in l1]

def get_label_from_proba(l1: list) -> list:
    label_list = ['entailment', 'neutral', 'contradiction']
    return label_list[np.argmax(l1)]

# Check samples whose nearest neighbors all have the same label except for it.
def _is_outlier_label(x: pd.Series, n_neighbors: int = 8) -> bool:
    _value_counts = df_train_snli_full[df_train_snli_full.index.isin(x.nearest_neighbors)].gold_label.value_counts()
    return (_value_counts.max() == n_neighbors - 1) and (_value_counts.idxmax() != x.gold_label)

# %% Load training dataset
df_train_snli_full = pd.read_json('../data/snli_1.0/snli_1.0_train.jsonl', lines=True, orient='records').rename({'pairID': 'guid'}, axis=1)
df_train_snli_full['guid'] = df_train_snli_full['guid'].apply(convert_string_to_unique_number)

# %% Load this data, because I computed the embeddings with the full dataset, with the NUll labels

D, I = np.load('models_1030_gpu_full/32nn.npy')
df_snli = pd.read_json('../data/snli_1.0/snli_1.0_train.jsonl', lines=True, orient='records')
df_snli = df_snli[['pairID', 'sentence1', 'sentence2', 'gold_label']]
df_snli['distances'] = D[:, 1:8].tolist()
df_snli['nearest_neighbors'] = I[:, 1:8].tolist()

# drop labels which were not used for training, i.e. the ones without gold_label.
df_snli = df_snli[df_snli.pairID.isin(df_train_snli.guid)]
df_snli = df_snli.rename({'pairID': 'guid'}, axis=1)
df_snli.guid = df_snli.guid.apply(convert_string_to_unique_number)

# load model and training dynamics
models_home_dir = "models_1030_gpu_full"
td_file_path = os.path.join(os.path.join(models_home_dir), "td_metrics.jsonl")
td_metrics = pd.read_json(
    td_file_path, 
    orient="records", 
    lines=True
)

# %% Define outlier labels.
preloaded = True
if not preloaded:
    df_snli['is_outlier_label'] = df_snli.progress_apply(
        lambda x: _is_outlier_label(x, n_neighbors=8), axis=1
    )
    df_snli[df_snli['is_outlier_label'] == True].to_pickle('df_snli_cls_outliers.pkl')
    df_snli[df_snli['is_outlier_label'] == True].to_csv('df_snli_cls_outliers.csv')
else:
    df_outliers = pd.read_pickle('../data/df_snli_cls_outliers.pkl')
    df_snli['is_outlier_label'] = df_snli.index.isin(df_outliers.index)

# Merge td_metrics with sentence and training data
td_metrics = td_metrics.merge(
    df_snli,
    how='left',
    on='guid'
)
td_metrics['prediction'] = td_metrics.probs.apply(get_label_from_proba)


# %%
scatter_it(td_metrics.groupby('gold_label').sample(1000), hue_metric='is_outlier_label')


# %%
td_metrics.groupby('gold_label').sample(1000).rename(
    columns={'variability': 'X1', 'confidence': 'X2'}
)[['X1', 'X2']].to_csv('../dashboard/backend/data/d3_datamap_test.csv', index=False)

#%% merge in metrics
df_snli_metrics = df_train_snli_full.merge(
    td_metrics[['guid', 'prediction', 'distances', 'nearest_neighbors', 'is_outlier_label', 'confidence', 'variability']], on='guid', how='left'
)
# # %% Create a datamap with the outliers from the clusters. 
scatter_it(td_metrics, title='SNLI_with_outliers', hue_metric='is_outlier_label', show_hist=True)

# # %%
# scatter_it(
#     pd.concat(
#     [
#         td_metrics.sample(2000),
#         td_metrics[td_metrics['is_outlier_neighbor']]
#     ]
# )
#     , title='SNLI_with_outliers', hue_metric='is_outlier_neighbor', show_hist=False)


# %% How strong is the overlap between hans-like samples and these ones?
df_snli_hans = pd.read_csv('snli_with_hans_heurstics.csv')
df_snli = df_snli.merge(df_snli_hans[['guid', 'hans_heuristics']], on='guid', how='left')
df_snli['is_hans_like'] = df_snli.apply(lambda x: any(literal_eval(x.hans_heuristics)), axis=1)

df_snli_metrics = df_snli_metrics.merge(df_snli_hans[['guid', 'hans_heuristics']], how='left', on='guid')
df_snli_metrics['is_hans_like'] = df_snli_metrics.apply(
    lambda x: any(literal_eval(x.hans_heuristics)) if pd.notna(x.hans_heuristics) else None, axis=1)

# %% Only a small portion is overlapping, but it is a bit correlated.
print(df_snli[df_snli['is_outlier_label'] == True].is_hans_like.value_counts(normalize=True))
print(df_snli.is_hans_like.value_counts(normalize=True))
print(df_snli[df_snli.is_hans_like].is_outlier_label.value_counts(normalize=True))
print(df_snli.is_outlier_label.value_counts(normalize=True))

# %%
df_outliers = df_snli_metrics[df_snli_metrics['is_outlier_label'] == True]
df_outliers['_other_sentences'] = df_outliers.nearest_neighbors.apply(get_neighbor_sentences)
df_outliers_inspect = pd.concat(
    [df_outliers[['sentence1', 'sentence2', 'gold_label', 'prediction']].reset_index(drop=True),
    pd.DataFrame(df_outliers['_other_sentences'].apply(
        lambda x: [item for sublist in x for item in sublist]
    ).tolist(), columns=[
                f'{i}_nn_{j+1}' for j in range(7) for i in ['sentence1', 'sentence2', 'gold_label', 'prediction']
            ])]
    , axis=1
)

# %%
df_outliers_inspect.apply(
    lambda x: x.gold_label == x.prediction, axis=1
).value_counts()



# %%
df_outliers_inspect[df_outliers_inspect.apply(
    lambda x: x.gold_label == x.prediction, axis=1
)].to_csv('correctly_predicted_outliers.csv')

# %%
df_outliers_inspect.iloc[0]

# %%
def get_misclassification_count(row: pd.Series):
    cols = (
        [f'{i}_nn_{j+1}' for j in range(7) for i in ['gold_label', 'prediction']]
    )
    return row[cols].value_counts()[0] < len(cols)  # return true if there are more misclassifieds.

df_outliers_inspect['misclassifications_in_cluster'] = (
    df_outliers_inspect
    .progress_apply(lambda x: get_misclassification_count(x), axis=1)
)

# %%
df_outliers_inspect[df_outliers_inspect['misclassifications_in_cluster']].to_csv('outlier_other_misclassifications_in_cluster.csv')

# %%
df_hans_outliers = df_snli_metrics[
    (df_snli_metrics['is_outlier_label'] == True) & (df_snli_metrics['is_hans_like'] == True)
]
df_hans_outliers['_other_sentences'] = df_hans_outliers.nearest_neighbors.apply(
    get_neighbor_sentences)
df_hans_outliers_inspect = pd.concat(
    [df_hans_outliers[['sentence1', 'sentence2', 'gold_label', 'prediction']].reset_index(drop=True),
    pd.DataFrame(df_hans_outliers['_other_sentences'].apply(
        lambda x: [item for sublist in x for item in sublist]
    ).tolist(), columns=[
                f'{i}_nn_{j+1}' for j in range(7) for i in ['sentence1', 'sentence2', 'gold_label', 'prediction']
            ])]
    , axis=1
)

df_hans_outliers_inspect.to_csv('hans_like_outliers.csv')

# %%
df_htl = df_snli_metrics.sort_values(['confidence', 'variability']).iloc[:1000].sample(100)
df_htl['_other_sentences'] = df_htl.nearest_neighbors.progress_apply(get_neighbor_sentences)

pd.concat(
    [
        df_htl[['sentence1', 'sentence2', 'gold_label', 'prediction']].reset_index(drop=True),
        pd.DataFrame(
            df_htl['_other_sentences'].apply(
                lambda x: [item for sublist in x for item in sublist]
            ).tolist(), columns=[
                f'{i}_nn_{j+1}' for j in range(7) for i in ['sentence1', 'sentence2', 'gold_label', 'prediction']
            ]
        )
    ],
    axis=1
)

# %%
df_snli_metrics.loc[11958][['sentence1', 'sentence2', 'gold_label', 'prediction']]

# %%
get_neighbor_sentences(df_snli_metrics.loc[11973].nearest_neighbors)


# %%
df_snli_metrics[
    df_snli_metrics['is_hans_like'] == True
].gold_label.value_counts(normalize=True)

# %%
df_snli_metrics[
    (df_snli_metrics['is_outlier_label'] == True) & (df_snli_metrics['is_hans_like'] == True)
    ].gold_label.value_counts(normalize=True)

# %%
df_snli_metrics[
    (df_snli_metrics['is_outlier_label'] == True) & (df_snli_metrics['is_hans_like'] == True)
    ].prediction.value_counts(normalize=True)


# %%
df_snli_metrics[
    (df_snli_metrics['is_outlier_label'] == True) & (df_snli_metrics['is_hans_like'] == True)
][['sentence1', 'sentence2', 'gold_label', 'prediction']]

# %%
df_htl = df_snli_metrics.sort_values(['confidence', 'variability']).iloc[:1000].sample(100)
df_htl['_other_sentences'] = df_htl.nearest_neighbors.progress_apply(get_neighbor_sentences)

pd.concat(
    [
        df_htl[['sentence1', 'sentence2', 'gold_label', 'prediction']].reset_index(drop=True),
        pd.DataFrame(
            df_htl['_other_sentences'].apply(
                lambda x: [item for sublist in x for item in sublist]
            ).tolist(), columns=[
                f'{i}_nn_{j+1}' for j in range(7) for i in ['sentence1', 'sentence2', 'gold_label', 'prediction']
            ]
        )
    ],
    axis=1
).to_csv('hard_to_learn_samples.csv')

# %%
df_outliers = df_snli_metrics[df_snli_metrics['is_outlier_label'] == True].sample(100)
df_outliers['_other_sentences'] = df_outliers.nearest_neighbors.progress_apply(get_neighbor_sentences)
pd.concat(
    [
        df_outliers[['sentence1', 'sentence2', 'gold_label', 'prediction']].reset_index(drop=True),
        pd.DataFrame(
            df_outliers['_other_sentences'].apply(
                lambda x: [item for sublist in x for item in sublist]
            ).tolist(), columns=[
                f'{i}_nn_{j+1}' for j in range(7) for i in ['sentence1', 'sentence2', 'gold_label', 'prediction']
            ]
        )
    ],
    axis=1
).to_csv('outlier_samples.csv')
# %%

# %%
import pandas as pd
import numpy as np
# %% load all of SNLI:
df_snli_full = pd.read_json('../data/snli_1.0/snli_1.0_train.jsonl', lines=True, orient='records').rename({'pairID': 'guid'}, axis=1)
examples = pd.read_csv('../data/htl_snli_samples.csv', index_col=0)
generations = pd.read_json('../scripts/output/generated_examples.json')

# %% create a dataframe in the same format as before, but with one entry per generation.
examples_dashboard = pd.read_csv(
    '../dashboard/backend/data/NLI/cartography_filtered/htl_dashboard_new.csv',
    index_col=0
    )
# %%
generations['original_pairID'] = generations.apply(lambda x:
    df_snli_full.guid.iloc[x.nearest_neighbors[0]], axis=1
)

# %%
examples_full = pd.read_pickle('htl_with_neighbors.pkl')
# %%
from tqdm import tqdm
tqdm.pandas()
df_snli_full_indexed = df_snli_full.set_index('guid')
examples_full['nn_querying_premises'] = examples_full.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence1']
        for pid in x.nn_querying_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)
examples_full['nn_querying_hypotheses'] = examples_full.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence2']
        for pid in x.nn_querying_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)

examples_full['nn_premises'] = examples_full.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence1']
        for pid in x.nearest_neighbors_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)
examples_full['nn_hypotheses'] = examples_full.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence2']
        for pid in x.nearest_neighbors_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)
# %%
generations[['original_pairID', 'premise', 'hypothesis']].merge(
    examples_full
)
# %%
examples_full[[

    
'nn_premises', 'nn_hypotheses'
]]
# %%
examples_dashboard.merge(
    examples_full[['pairID', 'nn_premises', 'nn_hypotheses', 'nn_querying_premises', 'nn_querying_hypotheses']],
    how='left', 
)
# %%
examples_full
# %%
generations.merge(
    pd.concat(
    [examples_dashboard.drop(['sentence1', 'sentence2', 'gold_label'], axis=1), examples_full[['pairID', 'nn_premises', 'nn_hypotheses', 'nn_querying_premises', 'nn_querying_hypotheses']]], axis=1
    ),
    how='left', left_on='original_pairID', right_on='pairID'
).rename({'premise': 'sentence1', 'hypothesis': 'sentence2', 'label': 'gold_label'},
axis=1)[['sentence1', 'sentence2', 'gold_label', 'nn_variability', 'nn_confidence',
'nn_querying_variability', 'nn_querying_confidence',
'nn_premises', 'nn_hypotheses', 'nn_querying_premises', 'nn_querying_hypotheses',
'suggestionRP', 'suggestionRP_label',
'suggestionRH', 'suggestionRH_label']].to_csv(
    'htl_dashboard_new.csv'
)
# %%
examples_full.nn_querying_premises.iloc[0]
# %%


# %%
import json
import pandas as pd
import random
from typing import List, Dict
import sys, inspect, os
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from tqdm import tqdm
tqdm.pandas()

# Import cartography modules.
from cartography.cartography.data_utils_glue import convert_string_to_unique_number


def _create_examples(snli_data_path,
                     two_class: bool = False,
                     n_hans_samples_per_class: int = 0,
                     hans_data_path: str = None,
                     seed: int = None) -> List[Dict]:
    """Creates examples for the training and dev sets."""
    examples = []
    with open(snli_data_path, 'r', encoding='utf-8') as file:
        for line in file:
            _json_obj = json.loads(line)
            guid = _json_obj['pairID']
            text_a_binary_parse = _json_obj['sentence1_binary_parse']
            text_a = _json_obj['sentence1']
            text_b = _json_obj['sentence2']
            label = _json_obj['gold_label']
            if (label == '-') or (label == ''):
                continue
            if two_class:
                if label == 'contradiction' or label == 'neutral':
                    label = 'non-entailment'
            examples.append(
                {
                    'guid': guid,
                    'sentence1_parse': text_a_binary_parse,
                    'sentence1': text_a,
                    'sentence2': text_b,
                    'label': label   
                })
        if n_hans_samples_per_class != 0:
            # Assert the SNLI samples are two-class. 
            if not two_class:
                raise ValueError('Hans samples are two-class. Set `two_class=True`.')
            # First, drop same-class samples randomly:
            random.seed(seed)
            _indices_del = (
                random.sample([i for i, sample_dict in enumerate(examples)
                    if sample_dict['label'] == 'entailment'], n_hans_samples_per_class) 
                + random.sample([i for i, sample_dict in enumerate(examples)
                    if sample_dict['label'] == 'non-entailment'], n_hans_samples_per_class)
            )
            # Load random samples from the HANS dataset equally from both classes.
            new_samples = (
                pd.read_json(
                    hans_data_path,
                    orient='records',
                    lines=True
                )
                .groupby('gold_label')
                .sample(n=n_hans_samples_per_class)
                .rename({'gold_label': 'label', 'pairID': 'guid'}, axis=1)
                [['guid', 'sentence1', 'sentence2', 'label']]
                .to_dict('records')
            )

            # Replace them directly.
            for i, new_sample in zip(_indices_del, new_samples):
                examples[i] = new_sample

    return examples 

def parse_phrase_list(parse, phrases):

    #print(parse)
    if parse == "":
        return phrases
    
    phrase_list = phrases

    words = parse.split()
    this_phrase = []
    next_level_parse = []
    for index, word in enumerate(words):
        if word == "(":
            next_level_parse += this_phrase
            this_phrase = ["("]

        elif word == ")" and len(this_phrase) > 0 and this_phrase[0] == "(":
            phrase_list.append(" ".join(this_phrase[1:]))
            next_level_parse += this_phrase[1:]
            this_phrase = []
        elif word == ")":
            next_level_parse += this_phrase
            next_level_parse.append(")")
            this_phrase = []
        else:
            this_phrase.append(word)

    return parse_phrase_list(" ".join(next_level_parse), phrase_list)


def _get_heuristics(premise: str, hypothesis: str, premise_parse: str):
    """HANS measure of lexical overlap, , as implemented in their github."""
    prem_words = []
    hyp_words = []

    for word in premise.split():
        if word not in [".", "?", "!"]:
            prem_words.append(word.lower())

    for word in hypothesis.split():
        if word not in [".", "?", "!"]:
            hyp_words.append(word.lower())

    parse_new = []
    for word in premise_parse.split():
        if word not in [".", "?", "!"]:
            parse_new.append(word.lower())

    all_phrases = parse_phrase_list(" ".join(parse_new), [])

    prem_filtered = " ".join(prem_words)
    hyp_filtered = " ".join(hyp_words)

    all_in = True
    for word in hyp_words:
        if word not in prem_words:
            all_in = False
            break

    return (all_in, hyp_filtered in prem_filtered, hyp_filtered in all_phrases)


def main():
    # Load original SNLI data.
    # snli_train_path = '/cluster/scratch/chanr/counterfactuals_data/SNLI/snli_1.0_train.jsonl'
    snli_train_path = '../data/snli_1.0/snli_1.0_train.jsonl'
    df_train_snli = pd.DataFrame(
        _create_examples(snli_train_path)
        )
    df_train_snli.guid = df_train_snli.guid.apply(convert_string_to_unique_number)

    df_train_snli['hans_heuristics'] = df_train_snli.progress_apply(lambda x: _get_heuristics(x.sentence1, x.sentence2, x.sentence1_parse), axis=1)
    df_train_snli.to_csv('snli_with_hans_heurstics.csv')

if __name__ == '__main__':
    main()

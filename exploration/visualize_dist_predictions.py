#%%
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

df_chaos = pd.read_csv('../data/sick/results100.csv', skiprows=1, index_col=0)

# post-process the results dataframe
df_chaos = pd.concat(
    [df_chaos.rename(
        {'E': 'chaos_E', 'N': 'chaos_N', 'C': 'chaos_C'}, axis=1
        )[['premise', 'hypothesis', 'chaos_E', 'chaos_N', 'chaos_C']]
        .reset_index(drop=True), 
    pd.DataFrame(df_chaos['as judge'].apply(
        lambda x: [int(item[0]) for item in x.split('|')[:-1]]
        ).tolist(), columns=['logical_E', 'logical_N', 'logical_C']),
    pd.DataFrame(df_chaos['as person on the street'].apply(
        lambda x: [int(item[0]) for item in x.split('|')[:-1]]
        ).tolist(), columns=['cs_E', 'cs_N', 'cs_C'])]
    , axis=1)

# Get the rows for which there is confusion.
chaos_cols = ['chaos_E', 'chaos_N', 'chaos_C']
logical_cols = ['logical_E', 'logical_N', 'logical_C']
cs_cols = ['cs_E', 'cs_N', 'cs_C']
df_chaos['l_neq_cs'] = df_chaos.apply(
    lambda x: 
    pd.to_numeric(x[logical_cols]).argmax() != pd.to_numeric(x[cs_cols]).argmax(),
    axis=1
)

# %%
def plot_joint_barplot(sp: pd.Series, show: bool = True) -> None:
    if isinstance(sp, pd.DataFrame):
        raise ValueError('Call .squeeze(axis=0) on 1-dimensional dataframe.')
    labels = ['entailment', 'netural', 'contradiction']
    chaos_probs = sp[[key for key in sp.keys() if 'chaos_' in key]].pipe(lambda x: x/x.sum()).values
    logical_probs = sp[[key for key in sp.keys() if 'logical_' in key]].pipe(lambda x: x/x.sum()).values
    cs_probs = sp[[key for key in sp.keys() if 'cs_' in key]].pipe(lambda x: x/x.sum()).values

    x = np.arange(len(labels))*2  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width, chaos_probs, width, label='chaos')
    rects2 = ax.bar(x, logical_probs, width, label='logical')
    rects3 = ax.bar(x + width, cs_probs, width, label='common-sense')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Estimated class probability')
    ax.set_title('Estimated class probability by label.')
    ax.set_xticks(x, labels)
    ax.legend(loc='upper center', bbox_to_anchor=(1.25, 1))

    ax.bar_label(rects1, padding=10)
    ax.bar_label(rects2, padding=10)
    ax.bar_label(rects3, padding=10)
    ax.set_ylim([0, 1])

    fig.tight_layout()
    if show:
        plt.show()

# %% joint barplot
random_lneqcs = df_chaos[df_chaos['l_neq_cs']].sample(1).squeeze(0)
plot_joint_barplot(random_lneqcs)

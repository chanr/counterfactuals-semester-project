# %%
import numpy as np
import pandas as pd
from scipy import spatial
from tqdm import tqdm

tqdm.pandas()

snli_full = pd.read_json('../data/snli_1.0/snli_1.0_train.jsonl', lines=True, orient='records').rename({'pairID': 'guid'}, axis=1)
examples = pd.read_csv('../data/htl_snli_samples.csv', index_col=0)
embeddings = np.load('../data/snli_train_embeddings.npy')
# %%
import ast

examples['nearest_neighbors_pairIDs'] = examples['nearest_neighbors_pairIDs'].apply(ast.literal_eval)
examples['nearest_neighbors_labels'] = examples['nearest_neighbors_labels'].apply(ast.literal_eval)

# %%
tree = spatial.KDTree(embeddings) 
# %%
nearest_neighbors = []
nearest_neighbors_equilabeled = []
for _, row in tqdm(examples.iterrows()):
    _got = False
    example_id = snli_full.index[snli_full.guid == row.pairID]
    embedding = embeddings[example_id] 
    for k in np.arange(4, 15):
        neighbor_ids = tree.query(embedding, k=2**k)[1].flatten()
        _df_neighbors = snli_full.loc[
            neighbor_ids
        ].loc[snli_full['gold_label'] == row.gold_label][:5]
        if len(_df_neighbors) == 5:
            print(2**k)
            if not 'nearest_neighbors_pairIDs' in row:
                nearest_neighbors.append(neighbor_ids[:8])
            nearest_neighbors_equilabeled.append(
                _df_neighbors.guid.tolist()
            )
            _got = True
            break  # we searched a large enough space.
    if not _got:
        raise ValueError('no thing founds')


# %% dirty, merge examples with nearest neighbors.
_nn_equi = pd.DataFrame(nearest_neighbors_equilabeled)
_nn_equi['nn_querying_pairIDs'] = _nn_equi[[0, 1, 2, 3, 4]].apply(lambda x: list(x), axis=1)
examples_new = examples.merge(
    _nn_equi[[0, 'nn_querying_pairIDs']], left_on='pairID', right_on=0, how='left'
).drop(0, axis=1)


if examples_new.iloc[0].pairID not in examples_new.iloc[0].nearest_neighbors_pairIDs:
    examples_new['nearest_neighbors_pairIDs'] = examples_new.apply(
        lambda x: [x.pairID] + x.nearest_neighbors_pairIDs, axis=1
    )
if examples_new.nearest_neighbors_labels.str.len().iloc[0] == 7:
    examples_new['nearest_neighbors_labels'] = examples_new.apply(
        lambda x: [x.gold_label] + x.nearest_neighbors_labels, axis=1
    )

# %% merge examples with snli metrics to get neighbor information, not necessarily in examples_new.
import pandas as pd
df_snli_metrics = pd.read_pickle('../data/df_snli_metrics.pkl')
df_snli_metrics_indexed = df_snli_metrics.set_index('pairID')

# %%
examples_new['nn_confidence'] = examples_new.progress_apply(
    lambda x: [df_snli_metrics_indexed.loc[pid, 'confidence']
        for pid in x.nearest_neighbors_pairIDs
        if pid in df_snli_metrics_indexed.index
    ], axis=1
)

examples_new['nn_variability'] = examples_new.progress_apply(
    lambda x: [df_snli_metrics_indexed.loc[pid, 'variability']
        for pid in x.nearest_neighbors_pairIDs
        if pid in df_snli_metrics_indexed.index
    ], axis=1
)

examples_new['nn_querying_confidence'] = examples_new.progress_apply(
    lambda x: [df_snli_metrics_indexed.loc[pid, 'confidence']
        for pid in x.nn_querying_pairIDs
        if pid in df_snli_metrics_indexed.index
    ], axis=1
)

examples_new['nn_querying_variability'] = examples_new.progress_apply(
    lambda x: [df_snli_metrics_indexed.loc[pid, 'variability']
        for pid in x.nn_querying_pairIDs
        if pid in df_snli_metrics_indexed.index
    ], axis=1
)

# %%
examples_new['suggestionRP'] = ""
examples_new['suggestionRP_label'] = ""
examples_new['suggestionRH'] = ""
examples_new['suggestionRH_label'] = ""

# %%
examples_new[['sentence1', 'sentence2', 'gold_label', 'nn_variability', 'nn_confidence',
'nearest_neighbors_labels',
'nn_querying_variability', 'nn_querying_confidence', 
'suggestionRP', 'suggestionRP_label',
'suggestionRH', 'suggestionRH_label']].to_csv(
    'htl_dashboard_new.csv'
)

# %%
examples_new.to_pickle('htl_dashboard_new.pkl')

# %% read in gpt-3 generations
generations = pd.read_json('../scripts/output/generated_examples.json')
generations['original_pairID'] = generations.apply(lambda x:
    snli_full.guid.iloc[x.nearest_neighbors[0]], axis=1
)
# %%
df_snli_full_indexed = snli_full.set_index('guid')
examples_new['nn_querying_premises'] = examples_new.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence1']
        for pid in x.nn_querying_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)
examples_new['nn_querying_hypotheses'] = examples_new.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence2']
        for pid in x.nn_querying_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)

examples_new['nn_premises'] = examples_new.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence1']
        for pid in x.nearest_neighbors_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)
examples_new['nn_hypotheses'] = examples_new.progress_apply(
    lambda x: [df_snli_full_indexed.loc[pid, 'sentence2']
        for pid in x.nearest_neighbors_pairIDs
        if pid in df_snli_full_indexed.index
    ], axis=1
)

# %%
generations.merge(
    pd.concat(
    [examples_new.drop(['sentence1', 'sentence2', 'gold_label'], axis=1)], axis=1
    ),
    how='left', left_on='original_pairID', right_on='pairID'
).rename({'premise': 'sentence1', 'hypothesis': 'sentence2', 'label': 'gold_label', 'nearest_neighbors_labels': 'nn_labels'},
axis=1)[['sentence1', 'sentence2', 'gold_label', 'nn_variability', 'nn_confidence',
'nn_querying_variability', 'nn_querying_confidence', 
'nn_premises', 'nn_hypotheses', 'nn_labels', 'nn_querying_premises', 'nn_querying_hypotheses',
'suggestionRP', 'suggestionRP_label',
'suggestionRH', 'suggestionRH_label']].to_pickle(
    '../dashboard/backend/data/NLI/cartography_filtered/htl_dashboard_new.pkl'
)


# %%

a.nn_labels.str.len().iloc[0]
# %%

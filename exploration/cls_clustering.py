# %%
import faiss
import os, sys, inspect
import numpy as np
from sklearn.metrics import silhouette_score

# %%
cls_embeddings = np.load('models_1030_gpu_full/snli_train_embeddings_0.npy')

# %%
silhouette_scores = []
n_centroids_list = [32, 64, 128, 256]
d = cls_embeddings.shape[1]
for n_centroids in n_centroids_list:
    kmeans = faiss.Kmeans(d, n_centroids, verbose=False)
    kmeans.train(cls_embeddings)
    print('Computing silhouette scores...')
    _, I = kmeans.index.search(cls_embeddings, 1)
    print(f'Appending silhouette scores for {n_centroids} centroids.')
    silhouette_scores.append(
        silhouette_score(cls_embeddings, np.ravel(I))
    )

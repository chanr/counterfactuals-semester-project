# %%
import inspect
import json
import os
import random
import sys
from typing import List, Dict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# from sentence_transformers import SentenceTransformer
# from sentence_transformers.util import community_detection
# import torch
import seaborn as sns
import spacy
nlp = spacy.load("en_core_web_sm")

# os/sys hack to import parent directory modules.
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

# Import cartography modules.
from cartography.cartography.data_utils_glue import convert_string_to_unique_number

# %%
def _create_examples(snli_data_path,
                     two_class: bool = False,
                     n_hans_samples_per_class: int = 0,
                     hans_data_path: str = None,
                     seed: int = None) -> List[Dict]:
    """Creates examples for the training and dev sets."""
    examples = []
    with open(snli_data_path, 'r', encoding='utf-8') as file:
        for line in file:
            _json_obj = json.loads(line)
            guid = _json_obj['pairID']
            text_a = _json_obj['sentence1']
            text_b = _json_obj['sentence2']
            label = _json_obj['gold_label']
            if (label == '-') or (label == ''):
                continue
            if two_class:
                if label == 'contradiction' or label == 'neutral':
                    label = 'non-entailment'
            examples.append(
                {
                    'guid': guid,
                    'sentence1': text_a,
                    'sentence2': text_b,
                    'label': label   
                })
        if n_hans_samples_per_class != 0:
            # Assert the SNLI samples are two-class. 
            if not two_class:
                raise ValueError('Hans samples are two-class. Set `two_class=True`.')
            # First, drop same-class samples randomly:
            random.seed(seed)
            _indices_del = (
                random.sample([i for i, sample_dict in enumerate(examples)
                    if sample_dict['label'] == 'entailment'], n_hans_samples_per_class) 
                + random.sample([i for i, sample_dict in enumerate(examples)
                    if sample_dict['label'] == 'non-entailment'], n_hans_samples_per_class)
            )
            # Load random samples from the HANS dataset equally from both classes.
            new_samples = (
                pd.read_json(
                    hans_data_path,
                    orient='records',
                    lines=True
                )
                .groupby('gold_label')
                .sample(n=n_hans_samples_per_class)
                .rename({'gold_label': 'label', 'pairID': 'guid'}, axis=1)
                [['guid', 'sentence1', 'sentence2', 'label']]
                .to_dict('records')
            )

            # Replace them directly.
            for i, new_sample in zip(_indices_del, new_samples):
                examples[i] = new_sample

    return examples 

# %%
def scatter_it(dataframe, hue_metric ='correct.', title='', model='RoBERTa', show_hist=False):
    # Subsample data to plot, so the plot is not too busy.
    dataframe = dataframe.sample(n=25000 if dataframe.shape[0] > 25000 else len(dataframe))
    
    # Normalize correctness to a value between 0 and 1.
    dataframe = dataframe.assign(corr_frac = lambda d: d.correctness / d.correctness.max())
    dataframe['correct.'] = [f"{x:.1f}" for x in dataframe['corr_frac']]
    
    main_metric = 'variability'
    other_metric = 'confidence'
    
    hue = hue_metric
    num_hues = len(dataframe[hue].unique().tolist())
    style = hue_metric if num_hues < 8 else None
    print(style)

    if not show_hist:
        fig, axs = plt.subplots(1, 1, figsize=(8, 4))
        ax0 = axs
    else:
        fig = plt.figure(figsize=(16, 10), )
        gs = fig.add_gridspec(2, 3, height_ratios=[5, 1])
    
        ax0 = fig.add_subplot(gs[0, :])
    
    
    ### Make the scatterplot.
    
    # Choose a palette.
    pal = sns.diverging_palette(260, 15, n=num_hues, sep=10, center="dark")

    plot = sns.scatterplot(x=main_metric,
                           y=other_metric,
                           ax=ax0,
                           data=dataframe,
                           hue=hue,
                           size='hans_type',
                           sizes={'non-hans': 5, 'hans': 100},
                           palette=pal,
                           style=style,
                           s=30)
    
    # Annotate Regions.
    bb = lambda c: dict(boxstyle="round,pad=0.3", ec=c, lw=2, fc="white")
    an1 = ax0.annotate("ambiguous", xy=(0.9, 0.5), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", rotation=350, bbox=bb('black'))
    an2 = ax0.annotate("easy-to-learn", xy=(0.27, 0.85), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", bbox=bb('r'))
    an3 = ax0.annotate("hard-to-learn", xy=(0.35, 0.25), xycoords="axes fraction", fontsize=15, color='black',
                  va="center", ha="center", bbox=bb('b'))
    
    if not show_hist:
        plot.legend(ncol=1, bbox_to_anchor=(1.01, 0.5), loc='center left', fancybox=True, shadow=True)
    else:
        plot.legend(fancybox=True, shadow=True,  ncol=1)
    plot.set_xlabel('variability')
    plot.set_ylabel('confidence')
    
    if show_hist:
        plot.set_title(f"{model}-{title} Data Map", fontsize=17)
        
        # Make the histograms.
        ax1 = fig.add_subplot(gs[1, 0])
        ax2 = fig.add_subplot(gs[1, 1])
        ax3 = fig.add_subplot(gs[1, 2])

        plott0 = dataframe.hist(column=['confidence'], ax=ax1, color='#622a87')
        plott0[0].set_title('')
        plott0[0].set_xlabel('confidence')
        plott0[0].set_ylabel('density')

        plott1 = dataframe.hist(column=['variability'], ax=ax2, color='teal')
        plott1[0].set_title('')
        plott1[0].set_xlabel('variability')

        plot2 = sns.countplot(x="correct.", data=dataframe, color='#86bf91', ax=ax3)
        ax3.xaxis.grid(True) # Show the vertical gridlines

        plot2.set_title('')
        plot2.set_xlabel('correctness')
        plot2.set_ylabel('')


    fig.tight_layout()
    # filename = f'figures/{title}_{model}.pdf' if show_hist else f'figures/compact_{title}_{model}.pdf'
    fig.savefig('20hans_samples.pdf', dpi=300)


# %% Load training dynamics data and merge with sentence information.
models_home_dir = "models_1027_gpu_hans"
td_file_path = os.path.join(os.path.join(models_home_dir), "td_metrics.jsonl")
td_metrics = pd.read_json(
    td_file_path, 
    orient="records", 
    lines=True
)

# Load original SNLI data.
df_train_snli = pd.DataFrame(
    _create_examples('../data/snli_1.0/snli_1.0_train.jsonl')
    )
df_train_snli.guid = df_train_snli.guid.apply(convert_string_to_unique_number)

# Load two-class version of! original SNLI training dataset. 
df_train_snli_two_class = pd.DataFrame(
    _create_examples('../data/snli_1.0/snli_1.0_train.jsonl', two_class=True)
    )
df_train_snli_two_class.guid = df_train_snli_two_class.guid.apply(convert_string_to_unique_number)

# Load two-class dataset with hans samples.
df_train_snli_hans = pd.DataFrame(
    _create_examples(snli_data_path='../data/snli_1.0/snli_1.0_train.jsonl',
                     two_class=True,
                     n_hans_samples_per_class=2,
                     hans_data_path='../data/hans/heuristics_evaluation_set.jsonl'
                     )
    )
df_train_snli_hans.guid = df_train_snli_hans.guid.apply(convert_string_to_unique_number)

# %%
df_hans_samples = pd.read_json(
    '../data/hans/heuristics_evaluation_set.jsonl',
    orient='records',
    lines=True
)
df_hans_samples.pairID = df_hans_samples.pairID.apply(convert_string_to_unique_number)
df_hans_samples = df_hans_samples.rename(columns={'pairID': 'guid', 'gold_label': 'label'})[['guid', 'sentence1', 'sentence2', 'label']]

# %% Add sentence information to the training metrics.
td_metrics = td_metrics.merge(
    pd.concat([df_hans_samples, df_train_snli_two_class]),
    how='left',
    on='guid'
)

# %% Label the samples which correspond to hans-like samples.
td_metrics['hans_type'] = 'non-hans'
td_metrics['hans_type'] = td_metrics.hans_type.where(
    ~td_metrics.guid.isin(df_hans_samples.guid),
    other='hans'
)
td_metrics['hans_non_entailment'] = 'non-hans'
td_metrics['hans_non_entailment'] = td_metrics.hans_non_entailment.where(
    ~(
        (td_metrics['hans_type'] != 'non-hans')
        & 
        (td_metrics.label != 'entailment')
    ),
    other='hans_non_entailment'
    ).where(
    ~(
        (td_metrics['hans_type'] != 'non-hans')
        & 
        (td_metrics.label == 'entailment')
    ),
    other='hans_entailment'
)
td_metrics.hans_non_entailment.value_counts(dropna=False)


# %%
scatter_it(
    pd.concat(
    [
        td_metrics[td_metrics['hans_type'] == 'non-hans'].sample(n=24980),
        td_metrics[td_metrics['hans_type'] == 'hans']
    ]
),
    title='SNLI',
    hue_metric='hans_non_entailment',
    show_hist=True
)

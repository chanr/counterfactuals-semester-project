"""
create a npy file containing [CLS] token embeddings for a given dataset of examples
"""

import pandas as pd
from transformers import RobertaTokenizer, RobertaForSequenceClassification
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm
import numpy as np
import torch
import os
import click
from pathlib import Path
import inspect
import sys

# os/sys hack to import parent directory modules.
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from cartography.cartography.classification.models import AdaptedRobertaForSequenceClassification


def ensure_dir(d):
    if not os.path.exists(d):
        os.makedirs(d)


def assign_GPU(Tokenizer_output):
    tokens_tensor = Tokenizer_output['input_ids'].to('cuda')
    token_type_ids = Tokenizer_output['token_type_ids'].to('cuda')
    attention_mask = Tokenizer_output['attention_mask'].to('cuda')

    output = {'input_ids' : tokens_tensor, 
          'token_type_ids' : token_type_ids, 
          'attention_mask' : attention_mask}

    return output


def get_cls_embedding(model, tokenizer, premise, hypothesis, mnli=True):
    if torch.cuda.is_available():
        x = assign_GPU(tokenizer.encode_plus(premise, hypothesis, return_tensors='pt', max_length=128, truncation=True))
    else:
        x = tokenizer.encode_plus(premise, hypothesis, return_tensors='pt', max_length=128, truncation=True)
    model_output = model(**x)
    if mnli:
        return torch.squeeze(model_output[-1], 0)
    else:  # snli
        assert(len(model_output[-1])==25) # assert we are actually looking at the hidden states
        return model_output[-1][-1][:,0,:]
    # outputs = model(**x, output_hidden_states=True)
    # return outputs.hidden_states[-1][:,0,:]


@click.command()
@click.option('--model_path', type=str, help='path to trained model')
@click.option('--data_file', type=str, help='jsonl file of examples to embed')
@click.option('--dataset_name', type=str, help='for naming output npy file')
def main(model_path: str, data_file: str, dataset_name: str):
    # model_path = Path(model_path)
    tokenizer = RobertaTokenizer.from_pretrained(model_path)
    nli_model = AdaptedRobertaForSequenceClassification.from_pretrained(model_path)
    if torch.cuda.is_available():
        nli_model = nli_model.to('cuda')
    
    model_path = Path(model_path)
    output_dir = model_path / 'representations'
    ensure_dir(output_dir)
    data_df = pd.read_json(data_file, lines=True, orient='records')
    batch_size = 50000
    num_batches = np.ceil(len(data_df.index) / batch_size).astype('int')
    print(f'Total number of batches: {num_batches}')

    # split into batches so we don't run out of memory
    for batch_idx in range(num_batches):
        batch = data_df[batch_idx * batch_size : min(len(data_df.index), (batch_idx+1) * batch_size)]
        with torch.no_grad():
            vectors = []
            for _, row in tqdm(batch.iterrows(), total=len(batch.index), desc=f'Batch {batch_idx}'):
                v = get_cls_embedding(nli_model, tokenizer, row['sentence1'], row['sentence2']).squeeze(0)
                vectors.append(v)

        vectors = torch.stack(vectors)
        vectors = torch.nn.functional.normalize(vectors)
        vectors = vectors.cpu().detach().numpy()
        
        # save to file
        with open(output_dir / f'{dataset_name}_{batch_idx}.npy', 'wb') as fo:
            np.save(fo, vectors)

    # combine batches
    vectors_per_batch = []
    for batch_idx in range(num_batches):
        with open(output_dir / f'{dataset_name}_{batch_idx}.npy', 'rb') as fin:
            vectors_per_batch.append(np.load(fin))

    mnli_vectors = np.concatenate(vectors_per_batch)
    with open(output_dir / f'{dataset_name}.npy', 'wb') as fo:
        np.save(fo, mnli_vectors)


if __name__ == '__main__':
    main()

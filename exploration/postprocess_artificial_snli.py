# %%
import pandas as pd

from utils import scatter_it, joint_it, create_examples, convert_string_to_unique_number

td_metrics = pd.read_json(
    'models_1012/td_metrics.jsonl',
    lines=True
)

#%% Load two-class version of! original SNLI training dataset. 
df_train_snli_two_class = pd.DataFrame(
    create_examples('../data/snli_1.0/snli_1.0_train.jsonl', two_class=True)
    )
df_train_snli_two_class.guid = df_train_snli_two_class.guid.apply(convert_string_to_unique_number)

# Load the samples which were flipped.
df_snli_etl = pd.read_pickle('../data/20_easiest_to_learn.pkl')

# %% load original data
df_hans_samples = pd.read_json(
    '../data/hans/heuristics_evaluation_set.jsonl',
    orient='records',
    lines=True
)
df_hans_samples.pairID = df_hans_samples.pairID.apply(convert_string_to_unique_number)
df_hans_samples = df_hans_samples.rename(columns={'pairID': 'guid', 'gold_label': 'label'})[['guid', 'sentence1', 'sentence2', 'label']]

# %% Add sentence information to the training metrics.
td_metrics = td_metrics.merge(
    pd.concat([df_hans_samples, df_train_snli_two_class]),
    how='left',
    on='guid'
)
td_metrics['flipped_etl'] = td_metrics.guid.isin(df_snli_etl.guid)

# %% add tag on the hans_type
td_metrics['hans_type'] = 'non-hans'
td_metrics['hans_type'] = td_metrics.hans_type.where(
    ~td_metrics.guid.isin(df_hans_samples.guid),
    other='hans'
)
# %% Flipped easy-to-learn in the datamap [wrongly classified samples]
joint_it(
    dataframe=pd.concat([
    td_metrics[~td_metrics['flipped_etl']].sample(
    1000 - len(td_metrics[td_metrics['flipped_etl']])),
    td_metrics[td_metrics['flipped_etl']]
    ]
), hue_metric='flipped_etl'
)

# %% hans / counterfactual examples.
scatter_it(
    dataframe=pd.concat([
    td_metrics[td_metrics['hans_type'] == 'non-hans'].sample(
    1000 - len(td_metrics[td_metrics['hans_type'] == 'hans'])),
    td_metrics[td_metrics['hans_type'] == 'hans']
    ]
), hue_metric='hans_type'
)

joint_it(
    dataframe=pd.concat([
    td_metrics[td_metrics['hans_type'] == 'non-hans'].sample(
    1000 - len(td_metrics[td_metrics['hans_type'] == 'hans'])),
    td_metrics[td_metrics['hans_type'] == 'hans']
    ]
), hue_metric='hans_type'
)
# %%
td_metrics[
    (td_metrics['hans_type'] == 'hans')
    &
    (td_metrics['label'] == 'non-entailment')
]
# %%

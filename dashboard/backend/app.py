
import os
from typing import List

from fastapi import FastAPI, UploadFile, File
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from AlignmentGraph import AlignmentGraph
import pandas as pd
import numpy as np

from pydantic_models.nli_data_point import NLIDataResponse, NLIDataSubmission, \
    NLISubmissionDisplay, NLISubmissionDisplayGraph

from typing import Callable
import pickle
import collatex

from transformers import RobertaTokenizer
from models import AdaptedRobertaForSequenceClassification

from roberta_inference  import roberta_inference, roberta_probability
from datamap_estimation import get_checkpoints, estimate_datamap_coordinates

app = FastAPI(
    title="Interactive Counterfactual Generation",
    description="""This is a dashboard for counterfactual generation tailored for NLI task.""",
    version="0.1.0",
)

# Allow CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

# adversarial
# data = pd.read_pickle('data/NLI/cartography_filtered/adversarial.pkl')
# print(data)
SUBMITTED_SAMPLES_PATH = 'data/NLI/submitted/cfs_submitted.tsv'
data = pd.read_pickle('data/NLI/cartography_filtered/htl_ambi_dashboard_2000.pkl')
_df_adv = pd.read_csv('data/NCF_samples.csv')
data = data[
    data.apply(
        lambda x: x.nn_querying_premises[0] in _df_adv.sentence1.tolist(),
        axis=1
    )
]
if len(data) == 0:
    raise ValueError(data)

# data = data.iloc[::-1]  ## reverse the data
# data = data.groupby(['sentence1', 'sentence2'], as_index=False).first()  ## Just for seed sample exploration.
# data = data.iloc[::10]

datamap_data = pd.read_csv('data/d3_datamap_test.csv')

grouped_data = data.groupby(["sentence1", "sentence2"])[["gold_label"]].count()
checkpoints, tokenizer = get_checkpoints(model_dir='data/models/models_0301_checkpoints/')
# checkpoints, tokenizer = {'test': 0}, None
model = list(checkpoints.values())[-1]

# import torch
# from transformers import RobertaTokenizer, RobertaForSequenceClassification
# model_test = RobertaForSequenceClassification.from_pretrained('alisawuffles/roberta-large-wanli')
# tokenizer = RobertaTokenizer.from_pretrained('alisawuffles/roberta-large-wanli')
# x = tokenizer("A wrestler is jumping off of the ring to hit his competitor.", "Two men are competing in a wrestling match.", return_tensors='pt', max_length=128, truncation=True)
# logits = model(**x).logits
# probs = logits.softmax(dim=1).squeeze(0)
# label_id = torch.argmax(probs).item()
# prediction = model.config.id2label[label_id]

# print(prediction)

print(roberta_inference('She is walking.', 'She is walking.', tokenizer, model))

# print(len(list(checkpoints.values())[::2]))

@app.get("/data-count")
def get_data_length() -> int:
    max_count = len(grouped_data)
    return max_count

@app.get('/datamap-coordinates')
def get_datamap_coordinates(sentence1: str, sentence2: str, label: str) -> str:
    return estimate_datamap_coordinates(
        sentence1, sentence2, label, list(checkpoints.values())[::2], tokenizer, use_less_checkpoints=False
    )

@app.get("/roberta-label")
def get_roberta_label(sentence1: str, sentence2: str) -> str:
    return roberta_inference(sentence1, sentence2, tokenizer, model)

@app.get('/get-data')
def upload_moons_data():
    return datamap_data.to_dict(orient='records')

@app.get("/upload-data", response_model=NLIDataResponse)
def upload_data(count: int):
    return [data.iloc[count].to_dict()]

SUBMITTED_SAMPLES_PATH = 'data/NLI/submitted/cfs_submitted.tsv'

@app.get("/upload-submitted-data", response_model=NLISubmissionDisplay)
def upload_submitted_data(sentence1: str, sentence2: str):

    if not os.path.exists(SUBMITTED_SAMPLES_PATH):
        df = pd.DataFrame(
            columns=['sentence1', 'sentence2', 'gold_label', 'suggestionRP', 'suggestionRH', 'annotator_label', 'Neutral', 'Entailment', 'Contradiction'])
        df.to_csv(SUBMITTED_SAMPLES_PATH)

    data = pd.read_csv(SUBMITTED_SAMPLES_PATH, sep="\t")
    matching_data = data[(data['sentence1'] == sentence1) & (data['sentence2'] == sentence2)]

    # compute robot label
    labels = ["Entailment", "Neutral", "Contradiction"]
    robot_labels = list()
    for index, row in matching_data.iterrows():
        probs = [row["Entailment"], row["Neutral"],row["Contradiction"]]
        label_index = probs.index(max(probs))
        label = labels[label_index]
        robot_labels.append(label)

    matching_data["Robot_Label"] = robot_labels
    # renaming here for the table in the frontend
    matching_data = matching_data.rename(
        columns={"suggestionRP": "New Premise", "suggestionRH": "New Hypothesis", "Robot_Label": "Robot Label", "annotator_label": "Human Label"})
    matching_data["id"] = matching_data.index + 1
    return matching_data[["id", "New Premise", "New Hypothesis", "Robot Label", "Human Label"]].to_dict(orient="records")


@app.get("/upload-submitted-graph", response_model=NLISubmissionDisplayGraph)
def upload_submitted_graph(sentence1: str, sentence2: str, labels: str):
    collation = collatex.core_classes.Collation()
    data = pd.read_csv(SUBMITTED_SAMPLES_PATH, sep="\t")
    # filter for lines with sentence1, sentence2 matching: added label checking here
    matching_data = data[(data['sentence1'] == sentence1) & (data['sentence2'] == sentence2)]
    # get gold label at this point:
    if not matching_data.empty:
        gold_label = matching_data['gold_label'].iloc[0]
    else:
        gold_label = ""

    matching_data = matching_data[matching_data['suggestionRH_label'].apply(lambda a: a in labels)]
    print(f'matching data : '
          f'{matching_data}')

    print(f'labels: '
          f'{labels}')
    # add original as id = 0 only add original if label fits
    collation_empty = True
    if gold_label in labels:
        collation.add_plain_witness(str(0), sentence2)
        collation_empty = False
    # add all matching counterfactuals
    if not matching_data.empty:
        for i, line in enumerate(matching_data['suggestionRH']):
            collation.add_plain_witness(str(i + 1), line)
            collation_empty = False

    if labels == "":
        collation.add_plain_witness(str(0), "Please select a label!")
        collation_empty = False
    elif collation_empty:
        collation.add_plain_witness(str(0), "Nothing here yet!")

    # collate to find matching parts

    alignment_table = collatex.core_functions.collate(collation, near_match=True,
                                                      segmentation=False, output="table")

    # return a tangled tree:
    # e.g. ordered list of lists one list is a column from the collation table
    graph = AlignmentGraph(alignment_table, len(collation.witnesses))

    # return probabilities for each sentence as a nested dictionary
    #list of probs is ["entailment, neutral, contradiction"]
    # also return pseudo probabilities of human annotator e.g. 1,0,0
    probabilities = list()
    if not matching_data.empty:
        for index,row in matching_data.iterrows():
            dic = dict()
            dic["id"] = row["suggestionRH"]
            dic["probs"] = [row["Entailment"],row["Neutral"],row["Contradiction"]]
            label = row["suggestionRH_label"]
            if label == "Entailment":
                dic["human_probs"] = [1.0, 0.0, 0.0]
            elif label == "Neutral":
                dic["human_probs"] = [0.0, 1.0, 0.0]
            elif label == "Contradiction":
                dic["human_probs"] = [0.0, 0.0, 1.0]

            probabilities.append(dic)

    print(f'probabilities: '
          f'{probabilities}')


    return [graph.levels_string, graph.occurrences, probabilities]


@app.get("/upload-embeddings-plot")
def upload_embeddings():
    # for now just read in the file and create a scatterplot
    records = np.load("data/hidden_states.npz", allow_pickle=True)["records"]
    records_df = pd.DataFrame.from_records(records)
    # read in umap embeddings
    infile = open("data/umap_mapper.pkl", "rb")
    umap = pickle.load(infile)
    embeddings = umap.embedding_
    embeddings_df = pd.DataFrame(embeddings, columns=["X1", "X2"])
    # join the two dataframes
    response = records_df.join(embeddings_df, on=None).reset_index(drop=True).drop(
        columns=['i'])
    return response.to_dict(orient="records")

@app.post("/submit-data")
async def submit_data(data_row: NLIDataSubmission):
    """
    Function receives a new submitted counterfactual and updates it in the submitted tsv file to store.
    """
    prob_dict = roberta_probability(data_row["suggestionRP"], data_row["suggestionRH"], tokenizer, model)
    new_data = pd.DataFrame.from_dict([data_row])

    new_data['Neutral'] = [prob_dict['neutral']]
    new_data['Entailment'] = [prob_dict['entailment']]
    new_data['Contradiction'] = [prob_dict['contradiction']]

    # we handle the duplicates here:
    if os.path.exists("data/NLI/submitted/cfs_submitted.tsv"):
        old_data = pd.read_csv("data/NLI/submitted/cfs_submitted.tsv", sep="\t")
        data = pd.concat([old_data, new_data], ignore_index=True).replace('', np.nan, regex=True,
                                                                      inplace=False)
    else: 
        data = new_data.replace('', np.nan, regex=True, inplace=False)
    data.drop_duplicates(["sentence1", "sentence2", "suggestionRP", "suggestionRH", "annotator_label"],inplace=True, ignore_index=True)
    data.to_csv(f"data/NLI/submitted/cfs_submitted.tsv", index=False, header=True,
                sep="\t")
    return True


@app.post("/delete-data")
async def delete_data(sentence1: str, sentence2: str, new_premise: str, new_hypothesis: str, label: str):
    old_data = pd.read_csv(SUBMITTED_SAMPLES_PATH, sep="\t")
    # find line(s) to delete and write new df
    print(old_data.shape)
    # print(old_data['sentence1'] == sentence1)
    # print(old_data['sentence2'] == sentence2)
    print(old_data['suggestionRH'])
    print(new_hypothesis)
    print(old_data['suggestionRH'] == new_hypothesis)
    # print(old_data['suggestionRP'] == new_premise)
    
    new_data = old_data.drop(
        old_data[(old_data["sentence1"] == sentence1)
        & (old_data["sentence2"] == sentence2)
        & (old_data["suggestionRH"] == new_hypothesis)
        & (old_data['suggestionRP'] == new_premise)
        & (old_data['annotator_label'] == label)
    ].index)
    print(new_data.shape)
    new_data.to_csv(SUBMITTED_SAMPLES_PATH, index=False, header=True,
                sep="\t")
    return True


@app.post("/files/")
async def create_file(file: bytes = File(...)):
    return {"file_size": len(file)}


@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    return {"filename": file.filename}


@app.get("/", response_class=HTMLResponse)
async def root():
    html_content = """
        <html>
            <head>
                <title>Week 2</title>
            </head>
            <body>
                <h1>Test Python Backend</h1>
                Visit the <a href="/docs">API doc</a> (<a href="/redoc">alternative</a>) for usage information.
            </body>
        </html>
        """
    return HTMLResponse(content=html_content, status_code=200)


def update_schema_name(app: FastAPI, function: Callable, name: str) -> None:
    """
    Updates the Pydantic schema name for a FastAPI function that takes
    in a fastapi.UploadFile = File(...) or bytes = File(...).

    This is a known issue that was reported on FastAPI#1442 in which
    the schema for file upload routes were auto-generated with no
    customization options. This renames the auto-generated schema to
    something more useful and clear.

    Args:
        app: The FastAPI application to modify.
        function: The function object to modify.
        name: The new name of the schema.
    """
    for route in app.routes:
        if route.endpoint is function:
            route.body_field.type_.__name__ = name
            break



update_schema_name(app, create_file, "CreateFileSchema")
update_schema_name(app, create_upload_file, "CreateUploadSchema")

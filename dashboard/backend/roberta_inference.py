import torch
import transformers
import numpy as np


def roberta_inference(sentence1: str, 
                    sentence2: str, 
                    tokenizer: transformers.RobertaTokenizerFast, 
                    model: transformers.RobertaForSequenceClassification):
    max_length = 512
    label_list = ['entailment', 'neutral', 'contradiction']

    encoded_input = tokenizer.encode_plus(sentence1, sentence2, add_special_tokens=True, max_length=max_length,)
    input_ids, token_type_ids, attention_mask = (
        encoded_input["input_ids"], encoded_input["token_type_ids"], 
        encoded_input['attention_mask']
    )
    # The mask has 1 for real tokens and 0 for padding tokens. Only real
    # tokens are attended to.
    padding_length = max_length - len(input_ids)

    input_ids = input_ids + ([0] * padding_length)
    attention_mask = attention_mask + ([0] * padding_length)
    token_type_ids = token_type_ids + ([0] * padding_length)

    all_input_ids = torch.tensor([input_ids], dtype=torch.long)
    all_attention_mask = torch.tensor([attention_mask], dtype=torch.long)

    logits = model(input_ids=all_input_ids, attention_mask=all_attention_mask)[0].detach()
    prediction: np.ndarray = torch.argmax(logits, dim=1).detach().cpu().numpy() ## returns prediction array
    return label_list[prediction[0]]


def roberta_probability(sentence1: str,
                        sentence2: str,
                        tokenizer: transformers.RobertaTokenizerFast,
                        model: transformers.RobertaForSequenceClassification):
    max_length = 512
    label_list = ['entailment', 'neutral', 'contradiction']

    encoded_input = tokenizer.encode_plus(sentence1, sentence2, add_special_tokens=True, max_length=max_length,)
    input_ids, token_type_ids, attention_mask = (
        encoded_input["input_ids"], encoded_input["token_type_ids"], 
        encoded_input['attention_mask']
    )
    # The mask has 1 for real tokens and 0 for padding tokens. Only real
    # tokens are attended to.
    padding_length = max_length - len(input_ids)

    input_ids = input_ids + ([0] * padding_length)
    attention_mask = attention_mask + ([0] * padding_length)
    token_type_ids = token_type_ids + ([0] * padding_length)

    all_input_ids = torch.tensor([input_ids], dtype=torch.long)
    all_attention_mask = torch.tensor([attention_mask], dtype=torch.long)

    logits = model(input_ids=all_input_ids, attention_mask=all_attention_mask)[0].detach()
    prob_dict = dict()
    for i in range(3):
        prob_dict[label_list[i]] = torch.squeeze(logits, 0)[i]

    print(prob_dict)
    return prob_dict
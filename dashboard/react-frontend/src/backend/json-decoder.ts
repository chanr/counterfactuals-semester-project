import { JsonDecoder } from 'ts.data.json';
import { NLIDataPoint } from "../types/NLIDataPoint";
import {NLIDataArray} from "../types/NLIDataArray";


const NLIDataPointDecoder = JsonDecoder.object<NLIDataPoint>(
    {
        sentence1: JsonDecoder.string,
        sentence2: JsonDecoder.string,
        gold_label: JsonDecoder.string,
        nn_variability: JsonDecoder.array<number>(JsonDecoder.number, 'number[]'),
        nn_confidence: JsonDecoder.array<number>(JsonDecoder.number, 'number[]'),
        nn_querying_variability: JsonDecoder.array<number>(JsonDecoder.number, 'number[]'),
        nn_querying_confidence: JsonDecoder.array<number>(JsonDecoder.number, 'number[]'),
        nn_premises: JsonDecoder.array<string>(JsonDecoder.string, 'string[]'),
        nn_hypotheses: JsonDecoder.array<string>(JsonDecoder.string, 'string[]'),
        nn_labels: JsonDecoder.array<string>(JsonDecoder.string, 'string[]'),
        nn_querying_premises: JsonDecoder.array<string>(JsonDecoder.string, 'string[]'),
        nn_querying_hypotheses: JsonDecoder.array<string>(JsonDecoder.string, 'string[]'),
        suggestionRP: JsonDecoder.string,
        suggestionRP_label: JsonDecoder.string,
        suggestionRH: JsonDecoder.string,
        suggestionRH_label: JsonDecoder.string,

    },
    'NLIDataPoint'
);

export const NLIDataPointArrayDecoder = JsonDecoder.array<NLIDataPoint>(NLIDataPointDecoder, 'NLIDataArray');


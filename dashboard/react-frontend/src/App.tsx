import React, {useEffect, useState} from 'react';
import './App.css';
import {queryBackendData, queryBackendData2, queryBackendEmbedding} from './backend/BackendQueryEngine';
import {queryBackendInt} from "./backend/BackendQueryEngine";
import Visualization from './Visualization';
import {NLIDataArray} from "./types/NLIDataArray";
import Box from '@mui/material/Box';
import TabContext from '@mui/lab/TabContext';
import TabPanel from '@mui/lab/TabPanel';

import {ThemeProvider, createTheme} from '@mui/material/styles';
import {NLIEmbeddingArray} from "./types/NLIEmbeddingArray";
import { DataArray } from './types/DataArray';


const theme = createTheme({
    palette: {
        primary: {
            main: '#91CCBE',
            light: '#f4f4f4'
        },
        secondary: {
            main: '#7f2c56',
        },
        error: {
            main: '#9e3030',
        },
        text: {
            primary: 'rgb(0,0,0)',
        },
    },
    typography: {
        fontFamily: 'Nunito',
    },
});

function App() {

const [scatterData, setScatterData] = useState<DataArray>([]);
const [exampleData, setExampleData] = useState<NLIDataArray>();
const [count, setCount] = useState(0);
const [totalCount, setTotalCount] = useState(0);
const [value, setValue] = React.useState('1');
const [Embeddings, setEmbeddings] = useState<NLIEmbeddingArray>();
const [maxCount, setMaxCount] = useState(0);

useEffect(() => {
    queryBackendInt(`data-count`).then((maxCount) => {
        setTotalCount(maxCount);
        setMaxCount(maxCount);
    });
    // queryBackendEmbedding('upload-embeddings-plot').then((response) => {
    //     setEmbeddings(response);
    // });
}, []);


useEffect(() => {
    queryBackendData(`upload-data?count=${count}`).then((exampleData) => {
        setExampleData(exampleData);
    });
}, [count]);

const a = 0 // update when a changes.
useEffect(() => {
    queryBackendData2(`get-data`).then((e) => {
        setScatterData(e)
    });
}, [a]);

const incrCount = () => {
    console.log(totalCount)
    if (count < totalCount - 1) {
        setCount(count + 1)
    }
};

const decrCount = () => {
    if (count > 0) {
        setCount(count - 1)
    }
};

return (
    <ThemeProvider theme={theme}>
            <Box sx={{width: '100%', typography: 'body1'}}>
                <TabContext value={value}>
                    <TabPanel value="1">{exampleData && scatterData && <Visualization
                        total_count={maxCount}
                        data={exampleData}
                        scatter_data={scatterData}
                        count={count}
                        incrCount={incrCount}
                        decrCount={decrCount}/>}</TabPanel>
                </TabContext>
            </Box>
    </ThemeProvider>
)   
}

export default App;

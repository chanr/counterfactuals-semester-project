# Semester Project Counterfactual Generation

[[_TOC_]]

## Team Members

1. Afra Amini
2. Robin Chan

## Project Description
Crowdsourcing is one of the major ways to create large scale datasets for various NLP tasks, including but not limited to natural language inference (NLI).  To create datasets for NLI tasks, crowd workers are shown a premise and asked to write three hypotheses that either entail the premise, contradict the premise or are neutral to it. Although such huge datasets have enabled state-of-the-art classifiers to be trained, previous research has shown that classifiers trained on such datasets rely on existing spurious correlations in the dataset to make predictions. While some previous works found adding couterfactually augmented data make classifiers less susceptible to spurious correlations [1], others have found such augmentations do not increase the generalization [2]. In this project we first look for properties of an augmentation schema that can lead to better generalization. We then aim to implement such schema in a human–in-the-loop framework to augment existing datasets at scale. 

**References:**

[1] Divyansh Kaushik, Eduard Hovy, and Zachary C Lipton. 2020. Learning the difference that makes a difference with counterfactually-augmented data. In ICLR.

[2] William Huang, Haokun Liu, and Samuel R. Bowman. 2020. Counterfactually-Augmented SNLI Training Data Does Not Yield Better Generalization Than Unaugmented Data. In Proceedings of the First Workshop on Insights from Negative Results in NLP, pages 82–87, Online. Association for Computational Linguistics.

## Installation Guide

1. Install poetry, a dependency management tool for python

```
curl -sSL https://install.python-poetry.org | python3.8 -
```

2. The environment (based on their `requirements.txt`) can be installed using my poetry file:

```
module load gcc/6.3.0 python/3.8.5  # If running on Euler.
poetry install
poetry shell
```

3. Download the SNLI dataset and model checkpoints. Note, this tries to download the dataset using the google drive client.
In case you run into issues, the checkpoints can also be downloaded directly from [here](https://drive.google.com/file/d/16kI2a3QadExbE7K8nCOvXv-FRFYS9U68/view?usp=share_link) and should be placed into `exploration/models_0301_checkpoints`.

```
chmod +x download_data.sh
./download_data.sh
```

4. Run frontend and backend:

```
# Launch backend, might take some time.
chmod +x launch_backend.sh
./launch_backend.sh

# In a separate terminal launch frontend.
chmod +x launch_frontend.sh
./launch_frontend.sh
```



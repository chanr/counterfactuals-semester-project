local CUDA_DEVICE = std.parseInt(std.extVar("CUDA_VISIBLE_DEVICES"));

local LEARNING_RATE = 1.0708609960508476e-05;
local BATCH_SIZE = 12;
local NUM_EPOCHS = 23;
local SEED = 93078;

local TASK = "SNLI";
local DATA_DIR = "../data/";
local FEATURES_CACHE_DIR = DATA_DIR + "/cache_" + SEED ;

local TEST = "/home/swabhas/data/glue/SNLI/diagnostic_test.tsv";

{
   "data_dir": DATA_DIR,
   "model_type": "roberta",
   "model_name_or_path": "roberta-large",
   "task_name": TASK,
   "seed": SEED,
   "num_train_epochs": NUM_EPOCHS,
   "learning_rate": LEARNING_RATE,
   "features_cache_dir": FEATURES_CACHE_DIR,
   "per_gpu_train_batch_size": BATCH_SIZE,
   "do_train": true,
   "do_eval": true,
   "do_test": false,
   "test": TEST,
   "patience": 3,
   "train": {
      "snli_data_path": DATA_DIR + "snli_1.0/snli_1.0_train.jsonl",
      "two_class": false,
      "n_hans_samples_per_class": 0,
      "hans_data_path": DATA_DIR + 'hans/heuristics_train_set.jsonl',
      "wanli_full_random_replacement": false,
      "wanli_data_path": DATA_DIR + 'wanli/train.jsonl',
      "remove_outliers": false,
      "outliers_data_path": DATA_DIR + "df_snli_cls_outliers.pkl",
      "flip_etl": false, 
      "etl_data_path": DATA_DIR + "20_easiest_to_learn.pkl",
      "sick": true,
      "sick_data_path": DATA_DIR + 'sick/sick-cured-original-train.csv',
      "seed": 0
   },
   "dev": {
      "snli_data_path": DATA_DIR + "snli_1.0/snli_1.0_dev.jsonl",
      "two_class": false,
      "n_hans_samples_per_class": 0,
      "hans_data_path": DATA_DIR + 'hans/heuristics_train_set.jsonl',
      "wanli_full_random_replacement": false,
      "wanli_data_path": DATA_DIR + 'wanli/test.jsonl',
      "remove_outliers": false,
      "outliers_data_path": DATA_DIR + "df_snli_cls_outliers.pkl",
      "flip_etl": false,
      "etl_data_path": DATA_DIR + "20_easiest_to_learn.pkl",
      "sick": true,
      "sick_data_path": DATA_DIR + "sick/sick-cured-original-eval.csv",
      "seed": 0
   }
}

#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=20
#SBATCH --gpus=1
#SBATCH --gres=gpumem:10240m
#SBATCH --time=48:00:00
#SBATCH --mem-per-cpu=12288

poetry shell
python -m cartography.classification.run_glue -c configs/mnli.jsonnet -o /cluster/scratch/chanr/counterfactuals_data/models_mnli --do_train


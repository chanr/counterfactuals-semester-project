import os, json, random
from typing import List, Dict
import pandas as pd
from tqdm import tqdm

from transformers.data.processors.utils import DataProcessor, InputExample
# os/sys hack to import parent directory modules.
import re

def convert_string_to_unique_number(string: str) -> int:
  """
  Hack to convert SNLI ID into a unique integer ID, for tensorizing.
  """
  id_map = {'e': '0', 'c': '1', 'n': '2'}

  # SNLI-specific hacks.
  if string.startswith('vg_len'):
    code = '555'
  elif string.startswith('vg_verb'):
    code = '444'
  else:
    code = '000'

  try:
    number = int(code + re.sub(r"\D", "", string) + id_map.get(string[-1], '3'))
  except:
    number = random.randint(10000, 99999)
  return number


class SNLIProcessor(DataProcessor):
    """Processor for the SNLI data set."""

    def get_labels(self, two_class: bool = False):
        if two_class:
            return ['entailment', 'non-entailment']
        else:
            return ["entailment", "neutral", "contradiction"]

    def _create_examples(self, args: Dict, set_type: str) -> List[Dict]:
        """Creates examples for the training and dev sets."""
        examples = []

        if args['sick']:
            return pd.read_csv(args['sick_data_path']).apply(
                lambda x:
                InputExample(
                    guid=str(x.guid),
                    text_a=x.sentence1,
                    text_b=x.sentence2,
                    label=x.gold_label
                ),
                axis=1
            ).to_list()

        if args['remove_outliers']:
            _outlier_guids = pd.read_pickle(
                    args['outliers_data_path']).guid.values
        if args['flip_etl']:
            print('Flipping 10 easiest to learn samples per class.')
            _etl_guids = pd.read_pickle(
                args['etl_data_path']).guid.values
        with open(args['snli_data_path'], 'r', encoding='utf-8') as file:
            for line in tqdm(file):
                _json_obj = json.loads(line)
                guid = _json_obj['pairID']
                text_a = _json_obj['sentence1']
                text_b = _json_obj['sentence2']
                label = _json_obj['gold_label']
                if (label == '-') or (label == ''):
                    continue
                if args['two_class']:
                    if label == 'contradiction' or label == 'neutral':
                        label = 'non-entailment'
                if args['flip_etl']:  # should also work for two-class
                    if convert_string_to_unique_number(guid) in _etl_guids:
                        label = random.choice(
                            [l for l in self.get_labels(two_class=args['two_class']) if l != label])
                if args['remove_outliers']:
                    if guid in _outlier_guids:
                        continue
                    else:
                        examples.append(
                    InputExample(guid=str(guid), text_a=text_a, text_b=text_b, label=label)
                        )
                else:
                    examples.append(
                        InputExample(guid=str(guid), text_a=text_a, text_b=text_b, label=label)
                            )

            if args['wanli_full_random_replacement']:
                wanli_samples = (
                    pd.read_json(args['wanli_data_path'],
                                orient='records',
                                lines=True
                                )
                    .drop(['pairID', 'genre'], axis=1)
                    .rename({'gold': 'label', 'id': 'guid', 'premise': 'sentence1', 'hypothesis': 'sentence2'}, axis=1)
                    [['guid', 'sentence1', 'sentence2', 'label']]
                )
                if args['two_class']:
                    raise ValueError('WaNLI replacement is based on 3-class SNLI!')
                random.seed(args['seed'])
                _indices_replace = (
                    random.sample([i for i, sample_dict in enumerate(examples)
                        if sample_dict.label == 'entailment'], wanli_samples.label.value_counts()['entailment'])
                    + random.sample([i for i, sample_dict in enumerate(examples)
                        if sample_dict.label == 'neutral'], wanli_samples.label.value_counts()['neutral'])
                    + random.sample([i for i, sample_dict in enumerate(examples)
                        if sample_dict.label == 'contradiction'], wanli_samples.label.value_counts()['contradiction'])
                )
                wanli_samples = [
                    InputExample(guid=str(wanli_sample['guid']),
                                text_a=wanli_sample['sentence1'],
                                text_b=wanli_sample['sentence2'],
                                label=wanli_sample['label']) for wanli_sample in wanli_samples.to_dict('records')]
                for i, wanli_sample in zip(_indices_replace, wanli_samples):
                    examples[i] = wanli_sample

            if args['n_hans_samples_per_class'] != 0:
                print(f"replacing {args['n_hans_samples_per_class']} samples per class with hans")
                # Assert the SNLI samples are two-class. 
                if not args['two_class']:
                    raise ValueError('Hans samples are two-class. Set `two_class=True`.')
                # First, drop same-class samples randomly:
                random.seed(args['seed'])
                _indices_replace = (
                    random.sample([i for i, sample_dict in enumerate(examples)
                        if sample_dict.label == 'entailment'], args['n_hans_samples_per_class']) 
                    + random.sample([i for i, sample_dict in enumerate(examples)
                        if sample_dict.label == 'non-entailment'], args['n_hans_samples_per_class'])
                )
                # Load random samples from the HANS dataset equally from both classes.
                hans_samples = (
                    pd.read_json(
                        args['hans_data_path'],
                        orient='records',
                        lines=True
                    )
                    .groupby('gold_label')
                    .sample(n=args['n_hans_samples_per_class'])
                    .rename({'gold_label': 'label', 'pairID': 'guid'}, axis=1)
                    [['guid', 'sentence1', 'sentence2', 'label']]
                    .to_dict('records')
                )
                hans_samples = [
                    InputExample(guid=str(new_sample['guid']),
                                text_a=new_sample['sentence1'],
                                text_b=new_sample['sentence2'],
                                label=new_sample['label']) for new_sample in hans_samples]
                # Replace them directly.
                for i, new_sample in zip(_indices_replace, hans_samples):
                    examples[i] = new_sample
            return examples

    def get_examples(self, args, set_type):
        return self._create_examples(args, set_type)

    def get_train_examples(self, args):
        """See base class."""
        return self.get_examples(args, "train")

    def get_dev_examples(self, args):
        """See base class."""
        return self.get_examples(args, "dev")

    def get_test_examples(self, args):
        """See base class."""
        return self.get_examples(args, "test")

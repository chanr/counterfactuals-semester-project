import pandas as pd
from transformers.data.processors.glue import MnliProcessor, MnliMismatchedProcessor, InputExample

class AdaptedMnliProcessor(MnliProcessor):
  def get_examples(self, data_file, set_type):
      # return self._create_examples(self._read_tsv(data_file), set_type=set_type)
      _df = pd.read_csv(data_file, sep='\t')
      _df['guid'] = _df.index.to_series().apply(lambda x: "%s-%s" % (set_type, x))
      return _df.apply(lambda x: InputExample(
          guid=x.guid,
          text_a=x.sentence1,
          text_b=x.sentence2,
          label=x.gold_label
      ), axis=1).tolist()


class AdaptedMnliMismatchedProcessor(MnliMismatchedProcessor):
  def get_examples(self, data_file, set_type):
      return self._create_examples(self._read_tsv(data_file), set_type=set_type)